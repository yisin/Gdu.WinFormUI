﻿using System;

namespace Gdu.WinFormUI
{
    public enum ButtonImageAlignment
    {
        Left,
        Top,
        Right,
        Bottom
    }
}
