﻿using System;

namespace Gdu.WinFormUI
{
    public enum ResizeGridLocation
    {
        TopLeft,
        TopRight,
        BottomLeft,
        BottomRight
    }
}
