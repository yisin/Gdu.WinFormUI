﻿using System;

namespace Gdu.WinFormUI
{
    public enum GlassPosition
    {
        Fill,
        Top,
        Right,
        Left,
        Bottom,
    }
}
