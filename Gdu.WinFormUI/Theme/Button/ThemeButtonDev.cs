﻿using System;
using System.Drawing;

namespace Gdu.WinFormUI
{
    public class ThemeButtonDev : GMButtonThemeBase
    {
        public ThemeButtonDev()
        {
            RoundedRadius = 0;

            ColorTable.ForeColorNormal = ColorTable.ForeColorHover =
                ColorTable.ForeColorPressed = Color.FromArgb(68, 68, 68);

            ColorTable.BorderColorNormal = Color.FromArgb(171, 171, 171);
            ColorTable.BorderColorHover = Color.FromArgb(126, 180, 234);
            ColorTable.BorderColorPressed = Color.FromArgb(98, 162, 228);

            ColorTable.BackColorNormal = Color.FromArgb(238, 238, 238);
            ColorTable.BackColorHover = Color.FromArgb(227, 239, 252);
            ColorTable.BackColorPressed = Color.FromArgb(212, 233, 252);
        }
    }
}
