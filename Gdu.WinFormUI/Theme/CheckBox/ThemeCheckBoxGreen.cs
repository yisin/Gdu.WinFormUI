﻿using System;
using System.Drawing;
using Gdu.WinFormUI.MyGraphics;

namespace Gdu.WinFormUI
{
    public class ThemeCheckBoxGreen: GMCheckBoxThemeBase
    {
        public ThemeCheckBoxGreen()
        {
            CheckRectColor = Color.FromArgb(122, 192, 120);
            CheckRectColorDisabled = Color.FromArgb(208, 231, 207);

            CheckFlagColor = Color.FromArgb(72, 150, 70);
            CheckFlagColorDisabled = Color.FromArgb(178, 185, 177);

            CheckRectBackColorNormal = Color.FromArgb(224, 240, 223);
            CheckRectBackColorDisabled = Color.FromArgb(233, 243, 232);
            CheckRectBackColorHighLight = Color.FromArgb(220, 238, 219);
            CheckRectBackColorPressed = ColorHelper.GetDarkerColor(CheckRectBackColorNormal, 6);
        }
    }
}
