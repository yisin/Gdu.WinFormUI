﻿using System;
using System.Drawing;

namespace Gdu.WinFormUI
{
    public class ThemeComboBoxXMPBlack : GMComboBoxThemeBase
    {
        public ThemeComboBoxXMPBlack()
        {
            TextBoxMarginNotEditable = new System.Windows.Forms.Padding(4, 0, 4, 0);
            RightButtonMargin = new System.Windows.Forms.Padding(2);
            //DrawBorder = false;
            BackColorNormal = Color.FromArgb(193, 193, 193);
            BackColorHover = Color.FromArgb(201, 201, 201);
            BorderColorNormal = BackColorNormal;
            BorderColorHover = BackColorHover;
            BorderRoundedRadius = 2;

            ComboTextColor = Color.FromArgb(55, 58, 61);

            RightButtonTheme = new ButtonTheme();
            ListBoxTheme = new ThemeListBoxXMPBlack();

            DropDownBorderColor = Color.FromArgb(32, 72, 116);
            DropDownBackColor = Color.FromArgb(76, 76, 76);
            ResizeGridColor = Color.FromArgb(120, 120, 120);

            //HowBackgroundRender = BackgroundStyle.LinearGradient;
            BackColorLG1Normal = Color.FromArgb(223, 223, 223);
            BackColorLG2Normal = Color.FromArgb(180, 180, 180);
            BackColorLG1Hover = Color.FromArgb(250, 252, 253);
            BackColorLG2Hover = Color.FromArgb(170, 209, 239);
        }

        private class ButtonTheme : GMButtonThemeBase
        {
            public ButtonTheme()
            {
                base.RoundedRadius = 0;
                ColorTable = this.GetColor();
            }

            private ButtonColorTable GetColor()
            {
                ButtonColorTable table = new ButtonColorTable();

                table.ForeColorNormal = Color.FromArgb(81, 85, 89);
                table.ForeColorHover = Color.FromArgb(101, 105, 104);
                table.ForeColorPressed = Color.FromArgb(35, 97, 199);

                return table;
            }
        }
    }
}
