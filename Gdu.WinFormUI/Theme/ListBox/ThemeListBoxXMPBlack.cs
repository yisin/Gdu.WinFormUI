﻿using System;
using System.Drawing;

namespace Gdu.WinFormUI
{
    public class ThemeListBoxXMPBlack : GMListBoxThemeBase
    {
        public ThemeListBoxXMPBlack()
        {
            InnerPaddingWidth = 2;
            ScrollBarTheme = new ThemeScrollbarXMPBlack();

            BackColor = Color.FromArgb(56, 56, 56);
            BorderColor = Color.FromArgb(32, 72, 116);

            DrawBorder = true;
            DrawInnerBorder = false;

            TextColor = Color.FromArgb(211, 211, 211);

            SelectedItemBackColor = Color.FromArgb(9, 78, 142);
            SelectedItemTextColor = TextColor;
            SelectedItemBorderColor = Color.FromArgb(13, 48, 89);

            HighLightBackColor = Color.FromArgb(120, SelectedItemBackColor);
            HighLightBorderColor = SelectedItemBorderColor;
            HighLightTextColor = TextColor;
        }
    }
}
