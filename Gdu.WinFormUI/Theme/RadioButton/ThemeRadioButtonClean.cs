﻿using System;
using System.Drawing;

namespace Gdu.WinFormUI
{
    public class ThemeRadioButtonClean : GMRadioButtonThemeBase
    {
        public ThemeRadioButtonClean()
        {
            RadioMarkWidth = 13;
            InnerSpotInflate = 3;

            HighLightOutterCircle = false;
            ShowGlassOnInnerSpot = false;

            OutterCircleColor = Color.FromArgb(75, 158, 235);
            InnerSpotColor = Color.FromArgb(94, 194, 25);

            RadioMarkBackColorHighLight = Color.FromArgb(246, 246, 246);
        }
    }
}
