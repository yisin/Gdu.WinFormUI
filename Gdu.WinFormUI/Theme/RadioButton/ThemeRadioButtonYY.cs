﻿using System;
using System.Drawing;

namespace Gdu.WinFormUI
{
    public class ThemeRadioButtonYY : GMRadioButtonThemeBase
    {
        public ThemeRadioButtonYY()
        {            
            RadioMarkWidth = 16;
            InnerSpotInflate = 4;
            HighLightOutterCircle = true;
            OutterCircleHighLightAlpha = 40;
            ShowGlassOnInnerSpot = true;

            OutterCircleColor = Color.Gray;
            InnerSpotColor = Color.FromArgb(109, 168, 16);

            RadioMarkBackColorPressed = Color.FromArgb(232, 232, 232);
        }
    }
}
