﻿using System;

namespace Gdu.WinFormUI
{
    /// <summary>
    /// this theme is just for chrome one quarter style only
    /// </summary>
    public class ThemeRollingBarChromeGreen : GMRollingBarThemeBase
    {
        public ThemeRollingBarChromeGreen()
        {
            Radius1 = 14;
            BaseColor = System.Drawing.Color.LightSeaGreen;
            BackColor = System.Drawing.Color.White;
        }
    }
}
