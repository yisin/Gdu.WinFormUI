﻿using System;
using System.Drawing;
using Gdu.WinFormUI.MyGraphics;

namespace Gdu.WinFormUI
{
    public class ThemeScrollbarDevStyle : ThemeScrollbarVS2013
    {
        public ThemeScrollbarDevStyle()
        {
            BestUndirectLen = 17;
            BackColor = Color.FromArgb(245, 245, 247);
            MiddleButtonOutterSpace2 = 5;
            SideButtonLength = 17;
            InnerPaddingWidth = 0;
            SideButtonForePathGetter = new ButtonForePathGetter(
                GraphicsPathHelper.Create7x4In7x7DownTriangleFlag);
            SideButtonForePathSize = new Size(7, 7);

            SideButtonTheme.ColorTable.ForeColorNormal = Color.FromArgb(128, 131, 143);
            SideButtonTheme.ColorTable.ForeColorHover = Color.FromArgb(32, 31, 53);
            SideButtonTheme.ColorTable.ForeColorPressed = Color.Black;

            SideButtonTheme.ColorTable.BackColorNormal = SideButtonTheme.ColorTable.BackColorHover =
                SideButtonTheme.ColorTable.BackColorPressed = SideButtonTheme.ColorTable.BackColorDisabled =
                BackColor;

            MdlButtonTheme.ColorTable.BorderColorNormal = MdlButtonTheme.ColorTable.BorderColorHover =
                MdlButtonTheme.ColorTable.BorderColorPressed = Color.FromArgb(169, 172, 181);
            MdlButtonTheme.ColorTable.BackColorNormal = Color.FromArgb(217, 218, 223);
            MdlButtonTheme.ColorTable.BackColorHover = Color.FromArgb(213, 224, 252);
            MdlButtonTheme.ColorTable.BackColorPressed = Color.FromArgb(202, 203, 205);

            MdlButtonTheme.RoundedRadius = 6;
        }
    }
}
