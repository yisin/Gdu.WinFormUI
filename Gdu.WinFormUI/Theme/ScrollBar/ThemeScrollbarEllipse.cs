﻿using System;
using Gdu.WinFormUI.MyGraphics;

namespace Gdu.WinFormUI
{
    public class ThemeScrollbarEllipse : GMScrollBarThemeBase
    {
        public ThemeScrollbarEllipse()
        {
            SideButtonBorderType = ButtonBorderType.Ellipse;
            MiddleButtonOutterSpace2 = 1;
            MiddleButtonOutterSpace1 = 0;
            MdlButtonTheme.RoundedRadius = 12;
            SideButtonLength = BestUndirectLen;

            SideButtonForePathSize = new System.Drawing.Size(7, 7);
            SideButtonForePathGetter = new ButtonForePathGetter(
                GraphicsPathHelper.Create7x4In7x7DownTriangleFlag);

            DrawExtraMiddleLine = true;
            ExtraMiddleLineColor = System.Drawing.Color.LightGray;
            ExtraMiddleLineLength = 5;
            BackColor = System.Drawing.Color.Transparent;
        }
    }
}
