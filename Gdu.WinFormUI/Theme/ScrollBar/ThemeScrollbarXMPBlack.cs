﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;

using Gdu.WinFormUI.MyGraphics;

namespace Gdu.WinFormUI
{
    public class ThemeScrollbarXMPBlack : GMScrollBarThemeBase
    {
        public ThemeScrollbarXMPBlack()
        {
            BackColor = Color.FromArgb(47, 47, 47);//(64, 64, 64);
            BorderColor = Color.FromArgb(47, 47, 47);
            DrawBackground = true;
            DrawBorder = true;

            InnerPaddingWidth = 1;
            MiddleButtonOutterSpace1 = 0;
            MiddleButtonOutterSpace2 = 1;
            SideButtonLength = 14;
            BestUndirectLen = 15;

            SideButtonForePathSize = new Size(7, 7);
            SideButtonForePathGetter = new ButtonForePathGetter(
                GraphicsPathHelper.Create7x4In7x7DownTriangleFlag);

            MdlButtonTheme.ColorTable = GetMiddleButtonColorTable();
            SideButtonTheme.ColorTable = GetSideButtonColorTable();

            DrawLinesInMiddleButton = true;
            MiddleButtonLine1Color = Color.FromArgb(42, 42, 42);
            MiddleButtonLine2Color = Color.FromArgb(95, 95, 95);

            MiddleBtnLineOutterSpace1 = 2;
            MiddleBtnLineOutterSpace2 = 2;
        }

        private ButtonColorTable GetMiddleButtonColorTable()
        {
            ButtonColorTable table = new ButtonColorTable();

            table.BackColorNormal = Color.FromArgb(87, 87, 87);
            table.BackColorHover = Color.FromArgb(110, 110, 110);
            table.BackColorPressed = Color.FromArgb(99, 99, 99);

            return table;
        }

        private ButtonColorTable GetSideButtonColorTable()
        {
            ButtonColorTable table = new ButtonColorTable();

            table.ForeColorNormal = Color.FromArgb(120, 120, 120);
            table.ForeColorHover = Color.FromArgb(140, 140, 140);
            table.ForeColorPressed = Color.FromArgb(160, 160, 160);
            table.ForeColorDisabled = Color.FromArgb(89, 89, 89);

            table.BackColorDisabled = table.BackColorNormal =
                table.BackColorPressed = table.BackColorHover = Color.FromArgb(47, 47, 47);//(64, 64, 64);

            return table;
        }
    }
}
