﻿namespace Gmei.ControlDemo
{
    partial class FormButtons
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gmCheckBox1 = new Gdu.WinFormUI.GMCheckBox();
            this.gmCheckBox2 = new Gdu.WinFormUI.GMCheckBox();
            this.gmCheckBox3 = new Gdu.WinFormUI.GMCheckBox();
            this.gmCheckBox4 = new Gdu.WinFormUI.GMCheckBox();
            this.gmCheckBox5 = new Gdu.WinFormUI.GMCheckBox();
            this.gmCheckBox6 = new Gdu.WinFormUI.GMCheckBox();
            this.gmCheckBox7 = new Gdu.WinFormUI.GMCheckBox();
            this.gmCheckBox8 = new Gdu.WinFormUI.GMCheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.gmRadioButton12 = new Gdu.WinFormUI.GMRadioButton();
            this.gmRadioButton11 = new Gdu.WinFormUI.GMRadioButton();
            this.gmRadioButton10 = new Gdu.WinFormUI.GMRadioButton();
            this.gmRadioButton9 = new Gdu.WinFormUI.GMRadioButton();
            this.gmButton1 = new Gdu.WinFormUI.GMButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.gmRadioButton3 = new Gdu.WinFormUI.GMRadioButton();
            this.gmRadioButton2 = new Gdu.WinFormUI.GMRadioButton();
            this.gmRadioButton4 = new Gdu.WinFormUI.GMRadioButton();
            this.gmRadioButton1 = new Gdu.WinFormUI.GMRadioButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.gmRadioButton8 = new Gdu.WinFormUI.GMRadioButton();
            this.gmRadioButton7 = new Gdu.WinFormUI.GMRadioButton();
            this.gmRadioButton6 = new Gdu.WinFormUI.GMRadioButton();
            this.gmRadioButton5 = new Gdu.WinFormUI.GMRadioButton();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // gmCheckBox1
            // 
            this.gmCheckBox1.Checked = true;
            this.gmCheckBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.gmCheckBox1.DrawFocusRect = true;
            this.gmCheckBox1.Image = null;
            this.gmCheckBox1.Location = new System.Drawing.Point(39, 59);
            this.gmCheckBox1.Name = "gmCheckBox1";
            this.gmCheckBox1.Size = new System.Drawing.Size(89, 21);
            this.gmCheckBox1.TabIndex = 0;
            this.gmCheckBox1.Text = "开机时启动";
            this.gmCheckBox1.UseVisualStyleBackColor = true;
            // 
            // gmCheckBox2
            // 
            this.gmCheckBox2.Image = null;
            this.gmCheckBox2.Location = new System.Drawing.Point(39, 86);
            this.gmCheckBox2.Name = "gmCheckBox2";
            this.gmCheckBox2.Size = new System.Drawing.Size(113, 21);
            this.gmCheckBox2.TabIndex = 1;
            this.gmCheckBox2.Text = "退出时删除记录";
            this.gmCheckBox2.UseVisualStyleBackColor = true;
            // 
            // gmCheckBox3
            // 
            this.gmCheckBox3.Image = null;
            this.gmCheckBox3.Location = new System.Drawing.Point(39, 113);
            this.gmCheckBox3.Name = "gmCheckBox3";
            this.gmCheckBox3.Size = new System.Drawing.Size(113, 21);
            this.gmCheckBox3.TabIndex = 2;
            this.gmCheckBox3.Text = "记住最新的选择";
            this.gmCheckBox3.ThreeState = true;
            this.gmCheckBox3.UseVisualStyleBackColor = true;
            // 
            // gmCheckBox4
            // 
            this.gmCheckBox4.Enabled = false;
            this.gmCheckBox4.Image = null;
            this.gmCheckBox4.Location = new System.Drawing.Point(39, 140);
            this.gmCheckBox4.Name = "gmCheckBox4";
            this.gmCheckBox4.Size = new System.Drawing.Size(77, 21);
            this.gmCheckBox4.TabIndex = 3;
            this.gmCheckBox4.Text = "下载优先";
            this.gmCheckBox4.UseVisualStyleBackColor = true;
            // 
            // gmCheckBox5
            // 
            this.gmCheckBox5.Checked = true;
            this.gmCheckBox5.CheckState = System.Windows.Forms.CheckState.Checked;
            this.gmCheckBox5.Enabled = false;
            this.gmCheckBox5.Image = null;
            this.gmCheckBox5.Location = new System.Drawing.Point(39, 167);
            this.gmCheckBox5.Name = "gmCheckBox5";
            this.gmCheckBox5.Size = new System.Drawing.Size(92, 21);
            this.gmCheckBox5.TabIndex = 4;
            this.gmCheckBox5.Text = "Auto Close";
            this.gmCheckBox5.UseVisualStyleBackColor = true;
            // 
            // gmCheckBox6
            // 
            this.gmCheckBox6.DrawFocusRect = true;
            this.gmCheckBox6.Image = null;
            this.gmCheckBox6.Location = new System.Drawing.Point(39, 213);
            this.gmCheckBox6.Name = "gmCheckBox6";
            this.gmCheckBox6.Size = new System.Drawing.Size(65, 21);
            this.gmCheckBox6.TabIndex = 5;
            this.gmCheckBox6.Text = "请选择";
            this.gmCheckBox6.UseVisualStyleBackColor = true;
            this.gmCheckBox6.CheckedChanged += new System.EventHandler(this.gmCheckBox6_CheckedChanged);
            // 
            // gmCheckBox7
            // 
            this.gmCheckBox7.Image = null;
            this.gmCheckBox7.Location = new System.Drawing.Point(39, 240);
            this.gmCheckBox7.Name = "gmCheckBox7";
            this.gmCheckBox7.Size = new System.Drawing.Size(98, 21);
            this.gmCheckBox7.TabIndex = 6;
            this.gmCheckBox7.Text = "New Theme";
            this.gmCheckBox7.ThreeState = true;
            this.gmCheckBox7.UseVisualStyleBackColor = true;
            // 
            // gmCheckBox8
            // 
            this.gmCheckBox8.CheckRectAlign = Gdu.WinFormUI.CheckMarkAlignment.Right;
            this.gmCheckBox8.Image = null;
            this.gmCheckBox8.Location = new System.Drawing.Point(39, 267);
            this.gmCheckBox8.Name = "gmCheckBox8";
            this.gmCheckBox8.Size = new System.Drawing.Size(89, 21);
            this.gmCheckBox8.TabIndex = 7;
            this.gmCheckBox8.Text = "拒绝大文件";
            this.gmCheckBox8.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.gmRadioButton12);
            this.panel1.Controls.Add(this.gmRadioButton11);
            this.panel1.Controls.Add(this.gmRadioButton10);
            this.panel1.Controls.Add(this.gmRadioButton9);
            this.panel1.Controls.Add(this.gmButton1);
            this.panel1.Location = new System.Drawing.Point(158, 192);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(294, 130);
            this.panel1.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(10, 101);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(276, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "上面的回收桶是通过GraphicsPath画的，不是图片";
            // 
            // gmRadioButton12
            // 
            this.gmRadioButton12.Image = null;
            this.gmRadioButton12.Location = new System.Drawing.Point(73, 8);
            this.gmRadioButton12.Name = "gmRadioButton12";
            this.gmRadioButton12.Size = new System.Drawing.Size(58, 21);
            this.gmRadioButton12.TabIndex = 4;
            this.gmRadioButton12.TabStop = true;
            this.gmRadioButton12.Text = "Right";
            this.gmRadioButton12.UseVisualStyleBackColor = true;
            this.gmRadioButton12.CheckedChanged += new System.EventHandler(this.gmRadioButton12_CheckedChanged);
            this.gmRadioButton12.Click += new System.EventHandler(this.gmRadioButton12_Click);
            // 
            // gmRadioButton11
            // 
            this.gmRadioButton11.Image = null;
            this.gmRadioButton11.Location = new System.Drawing.Point(214, 8);
            this.gmRadioButton11.Name = "gmRadioButton11";
            this.gmRadioButton11.Size = new System.Drawing.Size(71, 21);
            this.gmRadioButton11.TabIndex = 3;
            this.gmRadioButton11.TabStop = true;
            this.gmRadioButton11.Text = "Bottom";
            this.gmRadioButton11.UseVisualStyleBackColor = true;
            this.gmRadioButton11.Click += new System.EventHandler(this.gmRadioButton11_Click);
            // 
            // gmRadioButton10
            // 
            this.gmRadioButton10.Checked = true;
            this.gmRadioButton10.Image = null;
            this.gmRadioButton10.Location = new System.Drawing.Point(14, 8);
            this.gmRadioButton10.Name = "gmRadioButton10";
            this.gmRadioButton10.Size = new System.Drawing.Size(49, 21);
            this.gmRadioButton10.TabIndex = 2;
            this.gmRadioButton10.TabStop = true;
            this.gmRadioButton10.Text = "Left";
            this.gmRadioButton10.UseVisualStyleBackColor = true;
            this.gmRadioButton10.CheckedChanged += new System.EventHandler(this.gmRadioButton10_CheckedChanged);
            this.gmRadioButton10.Click += new System.EventHandler(this.gmRadioButton10_Click);
            // 
            // gmRadioButton9
            // 
            this.gmRadioButton9.Image = null;
            this.gmRadioButton9.Location = new System.Drawing.Point(147, 8);
            this.gmRadioButton9.Name = "gmRadioButton9";
            this.gmRadioButton9.Size = new System.Drawing.Size(51, 21);
            this.gmRadioButton9.TabIndex = 1;
            this.gmRadioButton9.TabStop = true;
            this.gmRadioButton9.Text = "Top";
            this.gmRadioButton9.UseVisualStyleBackColor = true;
            this.gmRadioButton9.Click += new System.EventHandler(this.gmRadioButton9_Click);
            // 
            // gmButton1
            // 
            this.gmButton1.ForeImage = null;
            this.gmButton1.ForeImageSize = new System.Drawing.Size(0, 0);
            this.gmButton1.ForePathAlign = Gdu.WinFormUI.ButtonImageAlignment.Left;
            this.gmButton1.ForePathGetter = null;
            this.gmButton1.ForePathSize = new System.Drawing.Size(0, 0);
            this.gmButton1.Image = null;
            this.gmButton1.Location = new System.Drawing.Point(98, 43);
            this.gmButton1.Name = "gmButton1";
            this.gmButton1.Size = new System.Drawing.Size(100, 29);
            this.gmButton1.SpaceBetweenPathAndText = 0;
            this.gmButton1.TabIndex = 0;
            this.gmButton1.Text = "清空已完成";
            this.gmButton1.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.gmRadioButton3);
            this.panel2.Controls.Add(this.gmRadioButton2);
            this.panel2.Controls.Add(this.gmRadioButton4);
            this.panel2.Controls.Add(this.gmRadioButton1);
            this.panel2.Location = new System.Drawing.Point(158, 59);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(171, 129);
            this.panel2.TabIndex = 9;
            // 
            // gmRadioButton3
            // 
            this.gmRadioButton3.Image = null;
            this.gmRadioButton3.Location = new System.Drawing.Point(17, 64);
            this.gmRadioButton3.Name = "gmRadioButton3";
            this.gmRadioButton3.Size = new System.Drawing.Size(76, 21);
            this.gmRadioButton3.TabIndex = 2;
            this.gmRadioButton3.TabStop = true;
            this.gmRadioButton3.Text = "自动全屏";
            this.gmRadioButton3.UseVisualStyleBackColor = true;
            // 
            // gmRadioButton2
            // 
            this.gmRadioButton2.DrawFocusRect = true;
            this.gmRadioButton2.Image = null;
            this.gmRadioButton2.Location = new System.Drawing.Point(17, 37);
            this.gmRadioButton2.Name = "gmRadioButton2";
            this.gmRadioButton2.Size = new System.Drawing.Size(124, 21);
            this.gmRadioButton2.TabIndex = 1;
            this.gmRadioButton2.TabStop = true;
            this.gmRadioButton2.Text = "记住上次播放位置";
            this.gmRadioButton2.UseVisualStyleBackColor = true;
            // 
            // gmRadioButton4
            // 
            this.gmRadioButton4.Checked = true;
            this.gmRadioButton4.Enabled = false;
            this.gmRadioButton4.Image = null;
            this.gmRadioButton4.Location = new System.Drawing.Point(17, 91);
            this.gmRadioButton4.Name = "gmRadioButton4";
            this.gmRadioButton4.Size = new System.Drawing.Size(76, 21);
            this.gmRadioButton4.TabIndex = 0;
            this.gmRadioButton4.TabStop = true;
            this.gmRadioButton4.Text = "循环播放";
            this.gmRadioButton4.UseVisualStyleBackColor = true;
            // 
            // gmRadioButton1
            // 
            this.gmRadioButton1.DrawFocusRect = true;
            this.gmRadioButton1.Image = null;
            this.gmRadioButton1.Location = new System.Drawing.Point(17, 10);
            this.gmRadioButton1.Name = "gmRadioButton1";
            this.gmRadioButton1.Size = new System.Drawing.Size(136, 21);
            this.gmRadioButton1.TabIndex = 0;
            this.gmRadioButton1.TabStop = true;
            this.gmRadioButton1.Text = "下载完成后自动关机";
            this.gmRadioButton1.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.gmRadioButton8);
            this.panel3.Controls.Add(this.gmRadioButton7);
            this.panel3.Controls.Add(this.gmRadioButton6);
            this.panel3.Controls.Add(this.gmRadioButton5);
            this.panel3.Location = new System.Drawing.Point(335, 59);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(168, 129);
            this.panel3.TabIndex = 10;
            // 
            // gmRadioButton8
            // 
            this.gmRadioButton8.Image = null;
            this.gmRadioButton8.Location = new System.Drawing.Point(16, 91);
            this.gmRadioButton8.Name = "gmRadioButton8";
            this.gmRadioButton8.RadioMarkAlign = Gdu.WinFormUI.CheckMarkAlignment.Right;
            this.gmRadioButton8.Size = new System.Drawing.Size(104, 21);
            this.gmRadioButton8.TabIndex = 4;
            this.gmRadioButton8.TabStop = true;
            this.gmRadioButton8.Text = "Align to right";
            this.gmRadioButton8.UseVisualStyleBackColor = true;
            // 
            // gmRadioButton7
            // 
            this.gmRadioButton7.Checked = true;
            this.gmRadioButton7.Enabled = false;
            this.gmRadioButton7.Image = null;
            this.gmRadioButton7.Location = new System.Drawing.Point(16, 64);
            this.gmRadioButton7.Name = "gmRadioButton7";
            this.gmRadioButton7.Size = new System.Drawing.Size(100, 21);
            this.gmRadioButton7.TabIndex = 3;
            this.gmRadioButton7.TabStop = true;
            this.gmRadioButton7.Text = "支持鼠标拖放";
            this.gmRadioButton7.UseVisualStyleBackColor = true;
            // 
            // gmRadioButton6
            // 
            this.gmRadioButton6.DrawFocusRect = true;
            this.gmRadioButton6.Image = null;
            this.gmRadioButton6.Location = new System.Drawing.Point(16, 37);
            this.gmRadioButton6.Name = "gmRadioButton6";
            this.gmRadioButton6.Size = new System.Drawing.Size(140, 21);
            this.gmRadioButton6.TabIndex = 2;
            this.gmRadioButton6.TabStop = true;
            this.gmRadioButton6.Text = "The Second Option";
            this.gmRadioButton6.UseVisualStyleBackColor = true;
            // 
            // gmRadioButton5
            // 
            this.gmRadioButton5.Image = null;
            this.gmRadioButton5.Location = new System.Drawing.Point(16, 10);
            this.gmRadioButton5.Name = "gmRadioButton5";
            this.gmRadioButton5.Size = new System.Drawing.Size(88, 21);
            this.gmRadioButton5.TabIndex = 1;
            this.gmRadioButton5.TabStop = true;
            this.gmRadioButton5.Text = "第一个选择";
            this.gmRadioButton5.UseVisualStyleBackColor = true;
            // 
            // FormButtons
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(520, 335);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.gmCheckBox8);
            this.Controls.Add(this.gmCheckBox7);
            this.Controls.Add(this.gmCheckBox6);
            this.Controls.Add(this.gmCheckBox5);
            this.Controls.Add(this.gmCheckBox4);
            this.Controls.Add(this.gmCheckBox3);
            this.Controls.Add(this.gmCheckBox2);
            this.Controls.Add(this.gmCheckBox1);
            this.Name = "FormButtons";
            this.Text = "Button, CheckBox, RadioButton演示";
            this.Load += new System.EventHandler(this.FormButtons_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Gdu.WinFormUI.GMCheckBox gmCheckBox1;
        private Gdu.WinFormUI.GMCheckBox gmCheckBox2;
        private Gdu.WinFormUI.GMCheckBox gmCheckBox3;
        private Gdu.WinFormUI.GMCheckBox gmCheckBox4;
        private Gdu.WinFormUI.GMCheckBox gmCheckBox5;
        private Gdu.WinFormUI.GMCheckBox gmCheckBox6;
        private Gdu.WinFormUI.GMCheckBox gmCheckBox7;
        private Gdu.WinFormUI.GMCheckBox gmCheckBox8;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private Gdu.WinFormUI.GMRadioButton gmRadioButton3;
        private Gdu.WinFormUI.GMRadioButton gmRadioButton2;
        private Gdu.WinFormUI.GMRadioButton gmRadioButton4;
        private Gdu.WinFormUI.GMRadioButton gmRadioButton1;
        private System.Windows.Forms.Panel panel3;
        private Gdu.WinFormUI.GMRadioButton gmRadioButton7;
        private Gdu.WinFormUI.GMRadioButton gmRadioButton6;
        private Gdu.WinFormUI.GMRadioButton gmRadioButton5;
        private Gdu.WinFormUI.GMRadioButton gmRadioButton8;
        private Gdu.WinFormUI.GMRadioButton gmRadioButton12;
        private Gdu.WinFormUI.GMRadioButton gmRadioButton11;
        private Gdu.WinFormUI.GMRadioButton gmRadioButton10;
        private Gdu.WinFormUI.GMRadioButton gmRadioButton9;
        private Gdu.WinFormUI.GMButton gmButton1;
        private System.Windows.Forms.Label label1;
    }
}