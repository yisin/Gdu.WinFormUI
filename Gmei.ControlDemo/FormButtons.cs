﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gdu.WinFormUI;
using Gdu.WinFormUI.MyGraphics;

namespace Gmei.ControlDemo
{
    public partial class FormButtons : GMForm
    {
        public FormButtons()
        {
            InitializeComponent();
        }

        private void FormButtons_Load(object sender, EventArgs e)
        {
            base.Size = new System.Drawing.Size(519, 333);

            ThemeCheckBoxGreen th = new ThemeCheckBoxGreen();

            gmCheckBox6.SetNewTheme(th);
            gmCheckBox7.SetNewTheme(th);
            gmCheckBox8.SetNewTheme(th);

            ThemeRadioButtonClean thc = new ThemeRadioButtonClean();

            gmRadioButton5.SetNewTheme(thc);
            gmRadioButton6.SetNewTheme(thc);
            gmRadioButton7.SetNewTheme(thc);
            gmRadioButton8.SetNewTheme(thc);

            ThemeRadioButtonYY thy = new ThemeRadioButtonYY();
            gmRadioButton9.SetNewTheme(thy);
            gmRadioButton10.SetNewTheme(thy);
            gmRadioButton11.SetNewTheme(thy);
            gmRadioButton12.SetNewTheme(thy);

            gmButton1.ForePathGetter = new ButtonForePathGetter(
                GraphicsPathHelper.Create15x15RecycleBin);
            gmButton1.ForePathSize = new Size(15, 15);

            gmButton1.SpaceBetweenPathAndText = 4;
        }

        private void gmCheckBox6_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void gmRadioButton10_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void gmRadioButton10_Click(object sender, EventArgs e)
        {
            gmButton1.ForePathAlign = ButtonImageAlignment.Left;
        }

        private void gmRadioButton9_Click(object sender, EventArgs e)
        {
            gmButton1.ForePathAlign = ButtonImageAlignment.Top;
        }

        private void gmRadioButton12_Click(object sender, EventArgs e)
        {
            gmButton1.ForePathAlign = ButtonImageAlignment.Right;
        }

        private void gmRadioButton11_Click(object sender, EventArgs e)
        {
            gmButton1.ForePathAlign = ButtonImageAlignment.Bottom;
        }

        private void gmRadioButton12_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
