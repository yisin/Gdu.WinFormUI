﻿namespace Gmei.ControlDemo
{
    partial class FormListBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gmListBox1 = new Gdu.WinFormUI.GMListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gmListBox2 = new Gdu.WinFormUI.GMListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.gmListBox3 = new Gdu.WinFormUI.GMListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbSelectionMode = new Gdu.WinFormUI.GMComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbHotTrack = new Gdu.WinFormUI.GMComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbFully = new Gdu.WinFormUI.GMComboBox();
            this.SuspendLayout();
            // 
            // gmListBox1
            // 
            this.gmListBox1.Location = new System.Drawing.Point(19, 47);
            this.gmListBox1.Name = "gmListBox1";
            this.gmListBox1.SelectedIndex = -1;
            this.gmListBox1.Size = new System.Drawing.Size(178, 449);
            this.gmListBox1.TabIndex = 0;
            this.gmListBox1.TopIndex = 0;
            this.gmListBox1.SelectedIndexChanged += new System.EventHandler(this.gmListBox1_SelectedIndexChanged);
            this.gmListBox1.SelectionChanged += new System.EventHandler(this.gmListBox1_SelectionChanged);
            this.gmListBox1.ItemClick += new Gdu.WinFormUI.ListItemClickHandler(this.gmListBox1_ItemClick);
            this.gmListBox1.ItemDoubleClick += new Gdu.WinFormUI.ListItemClickHandler(this.gmListBox1_ItemDoubleClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(394, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(188, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Event Log ( click here to clear )";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // gmListBox2
            // 
            this.gmListBox2.Location = new System.Drawing.Point(396, 63);
            this.gmListBox2.Name = "gmListBox2";
            this.gmListBox2.ScrollBarHShowMode = Gdu.WinFormUI.ScrollBarShowMode.Auto;
            this.gmListBox2.SelectedIndex = -1;
            this.gmListBox2.Size = new System.Drawing.Size(276, 196);
            this.gmListBox2.TabIndex = 2;
            this.gmListBox2.TopIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(394, 277);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Selected Items";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // gmListBox3
            // 
            this.gmListBox3.Location = new System.Drawing.Point(396, 299);
            this.gmListBox3.Name = "gmListBox3";
            this.gmListBox3.SelectedIndex = -1;
            this.gmListBox3.Size = new System.Drawing.Size(276, 196);
            this.gmListBox3.TabIndex = 4;
            this.gmListBox3.TopIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(215, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(129, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Item Selection Mode";
            // 
            // cmbSelectionMode
            // 
            this.cmbSelectionMode.DrawFocusRect = false;
            this.cmbSelectionMode.Editable = false;
            this.cmbSelectionMode.HotTrackItems = false;
            this.cmbSelectionMode.ItemDrawMode = Gdu.WinFormUI.ListDrawMode.AutoDraw;
            this.cmbSelectionMode.ItemHeight = 18;
            this.cmbSelectionMode.Location = new System.Drawing.Point(212, 64);
            this.cmbSelectionMode.Name = "cmbSelectionMode";
            this.cmbSelectionMode.SelectedIndex = -1;
            this.cmbSelectionMode.Size = new System.Drawing.Size(166, 22);
            this.cmbSelectionMode.TabIndex = 6;
            this.cmbSelectionMode.Text = null;
            this.cmbSelectionMode.SelectedIndexChanged += new System.EventHandler(this.cmbSelectionMode_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(214, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "Hot Track Item";
            // 
            // cmbHotTrack
            // 
            this.cmbHotTrack.DrawFocusRect = false;
            this.cmbHotTrack.DropDownResizable = false;
            this.cmbHotTrack.Editable = false;
            this.cmbHotTrack.HotTrackItems = false;
            this.cmbHotTrack.ItemDrawMode = Gdu.WinFormUI.ListDrawMode.AutoDraw;
            this.cmbHotTrack.ItemHeight = 18;
            this.cmbHotTrack.Location = new System.Drawing.Point(212, 130);
            this.cmbHotTrack.Name = "cmbHotTrack";
            this.cmbHotTrack.SelectedIndex = -1;
            this.cmbHotTrack.Size = new System.Drawing.Size(164, 22);
            this.cmbHotTrack.TabIndex = 8;
            this.cmbHotTrack.Text = null;
            this.cmbHotTrack.SelectedIndexChanged += new System.EventHandler(this.cmbHotTrack_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(216, 184);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(125, 17);
            this.label5.TabIndex = 9;
            this.label5.Text = "Top Item Fully Show";
            // 
            // cmbFully
            // 
            this.cmbFully.DrawFocusRect = false;
            this.cmbFully.DropDownResizable = false;
            this.cmbFully.Editable = false;
            this.cmbFully.HotTrackItems = true;
            this.cmbFully.ItemDrawMode = Gdu.WinFormUI.ListDrawMode.AutoDraw;
            this.cmbFully.ItemHeight = 18;
            this.cmbFully.Location = new System.Drawing.Point(212, 207);
            this.cmbFully.Name = "cmbFully";
            this.cmbFully.SelectedIndex = -1;
            this.cmbFully.Size = new System.Drawing.Size(164, 22);
            this.cmbFully.TabIndex = 10;
            this.cmbFully.Text = null;
            this.cmbFully.SelectedIndexChanged += new System.EventHandler(this.cmbFully_SelectedIndexChanged);
            // 
            // FormListBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(667, 488);
            this.Controls.Add(this.cmbFully);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cmbHotTrack);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cmbSelectionMode);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.gmListBox3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.gmListBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gmListBox1);
            this.Name = "FormListBox";
            this.Text = "ListBox And ComboBox Demo";
            this.Load += new System.EventHandler(this.FormListBox_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Gdu.WinFormUI.GMListBox gmListBox1;
        private System.Windows.Forms.Label label1;
        private Gdu.WinFormUI.GMListBox gmListBox2;
        private System.Windows.Forms.Label label2;
        private Gdu.WinFormUI.GMListBox gmListBox3;
        private System.Windows.Forms.Label label3;
        private Gdu.WinFormUI.GMComboBox cmbSelectionMode;
        private System.Windows.Forms.Label label4;
        private Gdu.WinFormUI.GMComboBox cmbHotTrack;
        private System.Windows.Forms.Label label5;
        private Gdu.WinFormUI.GMComboBox cmbFully;
    }
}