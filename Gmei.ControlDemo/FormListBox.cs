﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gdu.WinFormUI;
using Gdu.WinFormUI.MyGraphics;

namespace Gmei.ControlDemo
{
    public partial class FormListBox : GMForm
    {
        private void SetBlackTheme()
        {
            ThemeFormDevExpress thdv = new ThemeFormDevExpress();
            thdv.FormBackColor = Color.FromArgb(64, 64, 64);
            thdv.CaptionBackColorBottom = thdv.FormBackColor;
            thdv.CaptionBackColorTop = thdv.FormBackColor;
            thdv.FormBorderOutterColor = Color.FromArgb(64, 64, 64);
            thdv.FormBorderInnerColor = Color.FromArgb(100, 100, 100);
            thdv.MaxBoxColor.ForeColorNormal = Color.FromArgb(233, 233, 233);
            thdv.MinBoxColor.ForeColorNormal = Color.FromArgb(233, 233, 233);
            thdv.CaptionTextColor = Color.FromArgb(222, 222, 222);
            base.XTheme = thdv;

            Color fc = Color.FromArgb(211, 211, 211);
            label1.ForeColor = fc;
            label2.ForeColor = fc;
            label3.ForeColor = fc;
            label4.ForeColor = fc;
            label5.ForeColor = fc;
        }

        private void Add2Log(string s)
        {
            gmListBox2.Items.Add(s);
            gmListBox2.TopIndex = gmListBox2.Items.Count - 1;
        }

        public FormListBox()
        {
            InitializeComponent();
        }

        private void FormListBox_Load(object sender, EventArgs e)
        {
            SetBlackTheme();
            ThemeListBoxXMPBlack thb = new ThemeListBoxXMPBlack();
            gmListBox1.SetNewTheme(thb);
            gmListBox2.SetNewTheme(thb);
            gmListBox3.SetNewTheme(thb);

            ThemeComboBoxXMPBlack thc = new ThemeComboBoxXMPBlack();
            cmbSelectionMode.SetNewTheme(thc);
            cmbHotTrack.SetNewTheme(thc);
            cmbFully.SetNewTheme(thc);

            for (int i = 100000; i <= 100200; i++)
            {
                gmListBox1.Items.Add(i);
            }

            cmbSelectionMode.Items.Add("None");
            cmbSelectionMode.Items.Add("One");
            cmbSelectionMode.Items.Add("Multiple");
            cmbSelectionMode.SelectedIndex = 1;

            cmbHotTrack.Items.Add("False");
            cmbHotTrack.Items.Add("True");
            cmbHotTrack.SelectedIndex = 0;

            cmbFully.Items.Add("False");
            cmbFully.Items.Add("True");
            cmbFully.SelectedIndex = 1;
        }

        private void gmListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Add2Log("SelectedIndexChanged, new index: " + gmListBox1.SelectedIndex.ToString());
        }

        private void gmListBox1_SelectionChanged(object sender, EventArgs e)
        {
            Add2Log("SelectionChanged");
            object[] oa = gmListBox1.SelectedValues;
            gmListBox3.Items.Clear();
            for (int i = 0; i < oa.Length; i++)
            {
                gmListBox3.Items.Add(oa[i]);
            }
        }

        private void cmbSelectionMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cmbSelectionMode.SelectedIndex)
            {
                case 0:
                    gmListBox1.ItemSelectionMode = ListSelectionMode.None;
                    break;
                case 1:
                    gmListBox1.ItemSelectionMode = ListSelectionMode.One;
                    break;
                case 2:
                    gmListBox1.ItemSelectionMode = ListSelectionMode.Multiple;
                    break;
            }
        }

        private void gmListBox1_ItemClick(object sender, int index)
        {
            Add2Log("Click in index: " + index.ToString());
        }

        private void gmListBox1_ItemDoubleClick(object sender, int index)
        {
            Add2Log("Double-Click in index: " + index.ToString());
        }

        private void label1_Click(object sender, EventArgs e)
        {
            gmListBox2.Items.Clear();
        }

        private void cmbHotTrack_SelectedIndexChanged(object sender, EventArgs e)
        {
            gmListBox1.HighLightItemWhenMouseMove = (cmbHotTrack.SelectedIndex == 1);
        }

        private void cmbFully_SelectedIndexChanged(object sender, EventArgs e)
        {
            gmListBox1.TopItemFullyShow = (cmbFully.SelectedIndex == 1);
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
