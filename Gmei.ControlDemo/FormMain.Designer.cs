﻿namespace Gmei.ControlDemo
{
    partial class FormMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.gmhScrollBar1 = new Gdu.WinFormUI.GMHScrollBar();
            this.gmvScrollBar1 = new Gdu.WinFormUI.GMVScrollBar();
            this.gmTrackBar1 = new Gdu.WinFormUI.GMTrackBar();
            this.gmProgressBar1 = new Gdu.WinFormUI.GMProgressBar();
            this.gmRollingBar1 = new Gdu.WinFormUI.GMRollingBar();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblIndex = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblColor = new System.Windows.Forms.Label();
            this.colorBox = new Gdu.WinFormUI.GMComboBox();
            this.btnStart = new Gdu.WinFormUI.GMButton();
            this.btnStop = new Gdu.WinFormUI.GMButton();
            this.gmButton1 = new Gdu.WinFormUI.GMButton();
            this.gmButton2 = new Gdu.WinFormUI.GMButton();
            this.gmButton3 = new Gdu.WinFormUI.GMButton();
            this.gmButton4 = new Gdu.WinFormUI.GMButton();
            this.gmButton5 = new Gdu.WinFormUI.GMButton();
            this.gmButton6 = new Gdu.WinFormUI.GMButton();
            this.SuspendLayout();
            // 
            // gmhScrollBar1
            // 
            this.gmhScrollBar1.LargeChange = 100;
            this.gmhScrollBar1.Location = new System.Drawing.Point(22, 48);
            this.gmhScrollBar1.Maximum = 10000;
            this.gmhScrollBar1.MiddleButtonLengthPercentage = 5;
            this.gmhScrollBar1.Name = "gmhScrollBar1";
            this.gmhScrollBar1.Size = new System.Drawing.Size(638, 19);
            this.gmhScrollBar1.SmallChange = 1;
            this.gmhScrollBar1.TabIndex = 0;
            this.gmhScrollBar1.TabStop = false;
            this.gmhScrollBar1.ValueChanged += new System.EventHandler(this.gmhScrollBar1_ValueChanged);
            // 
            // gmvScrollBar1
            // 
            this.gmvScrollBar1.LargeChange = 2;
            this.gmvScrollBar1.Location = new System.Drawing.Point(22, 73);
            this.gmvScrollBar1.Maximum = 10;
            this.gmvScrollBar1.MiddleButtonLengthPercentage = 40;
            this.gmvScrollBar1.Name = "gmvScrollBar1";
            this.gmvScrollBar1.Size = new System.Drawing.Size(19, 243);
            this.gmvScrollBar1.SmallChange = 1;
            this.gmvScrollBar1.TabIndex = 1;
            this.gmvScrollBar1.TabStop = false;
            this.gmvScrollBar1.ValueChanged += new System.EventHandler(this.gmvScrollBar1_ValueChanged);
            // 
            // gmTrackBar1
            // 
            this.gmTrackBar1.Location = new System.Drawing.Point(70, 127);
            this.gmTrackBar1.Maximum = 100;
            this.gmTrackBar1.Name = "gmTrackBar1";
            this.gmTrackBar1.Size = new System.Drawing.Size(339, 46);
            this.gmTrackBar1.TabIndex = 2;
            this.gmTrackBar1.TickFrequency = 10;
            this.gmTrackBar1.TickSide = System.Windows.Forms.TickStyle.Both;
            this.gmTrackBar1.Value = 40;
            this.gmTrackBar1.XTheme = null;
            this.gmTrackBar1.ValueChanged += new System.EventHandler(this.gmTrackBar1_ValueChanged);
            // 
            // gmProgressBar1
            // 
            this.gmProgressBar1.Location = new System.Drawing.Point(70, 189);
            this.gmProgressBar1.Name = "gmProgressBar1";
            this.gmProgressBar1.Percentage = 40;
            this.gmProgressBar1.Shape = Gdu.WinFormUI.ProgressBarShapeStyle.Circle;
            this.gmProgressBar1.Size = new System.Drawing.Size(142, 127);
            this.gmProgressBar1.TabIndex = 3;
            this.gmProgressBar1.TabStop = false;
            this.gmProgressBar1.XTheme = null;
            // 
            // gmRollingBar1
            // 
            this.gmRollingBar1.Location = new System.Drawing.Point(276, 190);
            this.gmRollingBar1.Name = "gmRollingBar1";
            this.gmRollingBar1.RefleshFrequency = 90;
            this.gmRollingBar1.Size = new System.Drawing.Size(76, 63);
            this.gmRollingBar1.Style = Gdu.WinFormUI.RollingBarStyle.BigGuyLeadsLittleGuys;
            this.gmRollingBar1.TabIndex = 4;
            this.gmRollingBar1.TabStop = false;
            this.gmRollingBar1.XTheme = null;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(68, 91);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 12);
            this.label1.TabIndex = 7;
            this.label1.Text = "滚动条Value：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(157, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(11, 12);
            this.label2.TabIndex = 8;
            this.label2.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(440, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 13;
            this.label3.Text = "Index:";
            // 
            // lblIndex
            // 
            this.lblIndex.AutoSize = true;
            this.lblIndex.Location = new System.Drawing.Point(482, 103);
            this.lblIndex.Name = "lblIndex";
            this.lblIndex.Size = new System.Drawing.Size(11, 12);
            this.lblIndex.TabIndex = 14;
            this.lblIndex.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(512, 103);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 12);
            this.label5.TabIndex = 15;
            this.label5.Text = "Selected Color:";
            // 
            // lblColor
            // 
            this.lblColor.BackColor = System.Drawing.Color.White;
            this.lblColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblColor.Location = new System.Drawing.Point(613, 103);
            this.lblColor.Name = "lblColor";
            this.lblColor.Size = new System.Drawing.Size(47, 14);
            this.lblColor.TabIndex = 16;
            // 
            // colorBox
            // 
            this.colorBox.DrawFocusRect = false;
            this.colorBox.HotTrackItems = false;
            this.colorBox.ItemDrawMode = Gdu.WinFormUI.ListDrawMode.UserDraw;
            this.colorBox.ItemHeight = 18;
            this.colorBox.Location = new System.Drawing.Point(442, 127);
            this.colorBox.Name = "colorBox";
            this.colorBox.SelectedIndex = -1;
            this.colorBox.Size = new System.Drawing.Size(218, 23);
            this.colorBox.TabIndex = 17;
            this.colorBox.Text = null;
            this.colorBox.SelectedIndexChanged += new System.EventHandler(this.colorBox_SelectedIndexChanged);
            this.colorBox.DrawItem += new Gdu.WinFormUI.ListItemPaintHandler(this.colorBox_DrawItem);
            this.colorBox.MeasureItem += new System.Windows.Forms.MeasureItemEventHandler(this.colorBox_MeasureItem);
            this.colorBox.DrawComboText += new Gdu.WinFormUI.DrawComboTextEventHandler(this.colorBox_DrawComboText);
            // 
            // btnStart
            // 
            this.btnStart.DrawFocusRect = true;
            this.btnStart.ForeImage = null;
            this.btnStart.ForeImageSize = new System.Drawing.Size(0, 0);
            this.btnStart.ForePathAlign = Gdu.WinFormUI.ButtonImageAlignment.Left;
            this.btnStart.ForePathGetter = null;
            this.btnStart.ForePathSize = new System.Drawing.Size(0, 0);
            this.btnStart.Image = null;
            this.btnStart.Location = new System.Drawing.Point(232, 271);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(67, 29);
            this.btnStart.SpaceBetweenPathAndText = 0;
            this.btnStart.TabIndex = 18;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.DrawFocusRect = true;
            this.btnStop.ForeImage = null;
            this.btnStop.ForeImageSize = new System.Drawing.Size(0, 0);
            this.btnStop.ForePathAlign = Gdu.WinFormUI.ButtonImageAlignment.Left;
            this.btnStop.ForePathGetter = null;
            this.btnStop.ForePathSize = new System.Drawing.Size(0, 0);
            this.btnStop.Image = null;
            this.btnStop.Location = new System.Drawing.Point(305, 271);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(67, 29);
            this.btnStop.SpaceBetweenPathAndText = 0;
            this.btnStop.TabIndex = 19;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // gmButton1
            // 
            this.gmButton1.AutoSize = false;
            this.gmButton1.ForeImage = null;
            this.gmButton1.ForeImageSize = new System.Drawing.Size(0, 0);
            this.gmButton1.ForePathAlign = Gdu.WinFormUI.ButtonImageAlignment.Left;
            this.gmButton1.ForePathGetter = null;
            this.gmButton1.ForePathSize = new System.Drawing.Size(0, 0);
            this.gmButton1.Image = null;
            this.gmButton1.Location = new System.Drawing.Point(548, 184);
            this.gmButton1.Name = "gmButton1";
            this.gmButton1.Size = new System.Drawing.Size(112, 29);
            this.gmButton1.SpaceBetweenPathAndText = 0;
            this.gmButton1.TabIndex = 20;
            this.gmButton1.Text = "RollingBar";
            this.gmButton1.UseVisualStyleBackColor = true;
            this.gmButton1.Click += new System.EventHandler(this.gmButton1_Click);
            // 
            // gmButton2
            // 
            this.gmButton2.AutoSize = false;
            this.gmButton2.DrawFocusRect = true;
            this.gmButton2.ForeImage = null;
            this.gmButton2.ForeImageSize = new System.Drawing.Size(0, 0);
            this.gmButton2.ForePathAlign = Gdu.WinFormUI.ButtonImageAlignment.Left;
            this.gmButton2.ForePathGetter = null;
            this.gmButton2.ForePathSize = new System.Drawing.Size(0, 0);
            this.gmButton2.Image = null;
            this.gmButton2.Location = new System.Drawing.Point(430, 271);
            this.gmButton2.Name = "gmButton2";
            this.gmButton2.Size = new System.Drawing.Size(112, 29);
            this.gmButton2.SpaceBetweenPathAndText = 0;
            this.gmButton2.TabIndex = 21;
            this.gmButton2.Text = "ProgressBar";
            this.gmButton2.UseVisualStyleBackColor = true;
            this.gmButton2.Click += new System.EventHandler(this.gmButton2_Click);
            // 
            // gmButton3
            // 
            this.gmButton3.AutoSize = false;
            this.gmButton3.DrawFocusRect = true;
            this.gmButton3.ForeImage = null;
            this.gmButton3.ForeImageSize = new System.Drawing.Size(0, 0);
            this.gmButton3.ForePathAlign = Gdu.WinFormUI.ButtonImageAlignment.Left;
            this.gmButton3.ForePathGetter = null;
            this.gmButton3.ForePathSize = new System.Drawing.Size(0, 0);
            this.gmButton3.Image = null;
            this.gmButton3.Location = new System.Drawing.Point(430, 184);
            this.gmButton3.Name = "gmButton3";
            this.gmButton3.Size = new System.Drawing.Size(112, 29);
            this.gmButton3.SpaceBetweenPathAndText = 0;
            this.gmButton3.TabIndex = 22;
            this.gmButton3.Text = "ScrollBar";
            this.gmButton3.UseVisualStyleBackColor = true;
            this.gmButton3.Click += new System.EventHandler(this.gmButton3_Click);
            // 
            // gmButton4
            // 
            this.gmButton4.AutoSize = false;
            this.gmButton4.DrawFocusRect = true;
            this.gmButton4.ForeImage = null;
            this.gmButton4.ForeImageSize = new System.Drawing.Size(0, 0);
            this.gmButton4.ForePathAlign = Gdu.WinFormUI.ButtonImageAlignment.Left;
            this.gmButton4.ForePathGetter = null;
            this.gmButton4.ForePathSize = new System.Drawing.Size(0, 0);
            this.gmButton4.Image = null;
            this.gmButton4.Location = new System.Drawing.Point(430, 226);
            this.gmButton4.Name = "gmButton4";
            this.gmButton4.Size = new System.Drawing.Size(112, 29);
            this.gmButton4.SpaceBetweenPathAndText = 0;
            this.gmButton4.TabIndex = 23;
            this.gmButton4.Text = "TrackBar";
            this.gmButton4.UseVisualStyleBackColor = true;
            this.gmButton4.Click += new System.EventHandler(this.gmButton4_Click);
            // 
            // gmButton5
            // 
            this.gmButton5.AutoSize = false;
            this.gmButton5.ForeImage = null;
            this.gmButton5.ForeImageSize = new System.Drawing.Size(0, 0);
            this.gmButton5.ForePathAlign = Gdu.WinFormUI.ButtonImageAlignment.Left;
            this.gmButton5.ForePathGetter = null;
            this.gmButton5.ForePathSize = new System.Drawing.Size(0, 0);
            this.gmButton5.Image = null;
            this.gmButton5.Location = new System.Drawing.Point(548, 226);
            this.gmButton5.Name = "gmButton5";
            this.gmButton5.Size = new System.Drawing.Size(112, 29);
            this.gmButton5.SpaceBetweenPathAndText = 0;
            this.gmButton5.TabIndex = 24;
            this.gmButton5.Text = "Buttons";
            this.gmButton5.UseVisualStyleBackColor = true;
            this.gmButton5.Click += new System.EventHandler(this.gmButton5_Click);
            // 
            // gmButton6
            // 
            this.gmButton6.AutoSize = false;
            this.gmButton6.ForeImage = null;
            this.gmButton6.ForeImageSize = new System.Drawing.Size(0, 0);
            this.gmButton6.ForePathAlign = Gdu.WinFormUI.ButtonImageAlignment.Left;
            this.gmButton6.ForePathGetter = null;
            this.gmButton6.ForePathSize = new System.Drawing.Size(0, 0);
            this.gmButton6.Image = null;
            this.gmButton6.Location = new System.Drawing.Point(548, 271);
            this.gmButton6.Name = "gmButton6";
            this.gmButton6.Size = new System.Drawing.Size(112, 29);
            this.gmButton6.SpaceBetweenPathAndText = 0;
            this.gmButton6.TabIndex = 25;
            this.gmButton6.Text = "ListBox";
            this.gmButton6.UseVisualStyleBackColor = true;
            this.gmButton6.Click += new System.EventHandler(this.gmButton6_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(723, 408);
            this.Controls.Add(this.gmButton6);
            this.Controls.Add(this.gmButton5);
            this.Controls.Add(this.gmButton4);
            this.Controls.Add(this.gmButton3);
            this.Controls.Add(this.gmButton2);
            this.Controls.Add(this.gmButton1);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.colorBox);
            this.Controls.Add(this.lblColor);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblIndex);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gmRollingBar1);
            this.Controls.Add(this.gmProgressBar1);
            this.Controls.Add(this.gmTrackBar1);
            this.Controls.Add(this.gmvScrollBar1);
            this.Controls.Add(this.gmhScrollBar1);
            this.Name = "FormMain";
            this.Text = "GduUI类库控件使用演示";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Gdu.WinFormUI.GMHScrollBar gmhScrollBar1;
        private Gdu.WinFormUI.GMVScrollBar gmvScrollBar1;
        private Gdu.WinFormUI.GMTrackBar gmTrackBar1;
        private Gdu.WinFormUI.GMProgressBar gmProgressBar1;
        private Gdu.WinFormUI.GMRollingBar gmRollingBar1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblIndex;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblColor;
        private Gdu.WinFormUI.GMComboBox colorBox;
        private Gdu.WinFormUI.GMButton btnStart;
        private Gdu.WinFormUI.GMButton btnStop;
        private Gdu.WinFormUI.GMButton gmButton1;
        private Gdu.WinFormUI.GMButton gmButton2;
        private Gdu.WinFormUI.GMButton gmButton3;
        private Gdu.WinFormUI.GMButton gmButton4;
        private Gdu.WinFormUI.GMButton gmButton5;
        private Gdu.WinFormUI.GMButton gmButton6;
    }
}

