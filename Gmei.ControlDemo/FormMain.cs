﻿using System;
using System.Drawing;
using System.Windows.Forms;

using Gdu.WinFormUI;

namespace Gmei.ControlDemo
{
    public partial class FormMain : GMForm
    {
        public FormMain()
        {
            InitializeComponent();

            Array ary = Enum.GetValues(typeof(KnownColor));
            foreach (KnownColor color in ary)
            {
                colorBox.Items.Add(color);
            }
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            gmhScrollBar1.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right;
            gmvScrollBar1.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Bottom;

            base.XTheme = new ThemeFormDevExpress();

            var theme = new ThemeScrollbarDevStyle();
            gmvScrollBar1.SetNewTheme(theme);
            gmhScrollBar1.SetNewTheme(theme);
            gmvScrollBar1.Width = theme.BestUndirectLen;
            gmhScrollBar1.Height = theme.BestUndirectLen;

            gmProgressBar1.XTheme = new ThemeProgressBarSoftGreen();
            gmRollingBar1.XTheme = new ThemeRollingBarGuys();

            btnStop.Enabled = false;

            this.Size = new Size(682, 433);
        }

        private void gmTrackBar1_ValueChanged(object sender, EventArgs e)
        {
            gmProgressBar1.Percentage = gmTrackBar1.Value;
        }

        private void gmvScrollBar1_ValueChanged(object sender, EventArgs e)
        {
            label2.Text = gmvScrollBar1.Value.ToString();
        }

        private void gmhScrollBar1_ValueChanged(object sender, EventArgs e)
        {
            label2.Text = gmhScrollBar1.Value.ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            new FormScrollBar().Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            new FormTrackBar().Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            new FormRollingBar().Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            new FormProgressBar().Show();
        }

        private void colorBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblIndex.Text = colorBox.SelectedIndex.ToString();
            if (colorBox.SelectedIndex == -1)
                lblColor.BackColor = Color.Transparent;
            else
            {
                lblColor.BackColor = Color.FromKnownColor((KnownColor)colorBox.SelectedValue);
                gmRollingBar1.XTheme.BaseColor = lblColor.BackColor;
                gmRollingBar1.Refresh();
            }
        }

        private void colorBox_MeasureItem(object sender, MeasureItemEventArgs e)
        {
            Size size = TextRenderer.MeasureText(
                colorBox.Items[e.Index].ToString(), colorBox.XTheme.ListBoxTheme.TextFont);
            e.ItemHeight = size.Height;
            e.ItemWidth = size.Width + 30;
        }

        private void colorBox_DrawItem(object sender, ItemPaintEventArgs e)
        {
            Rectangle rect = e.ItemRect;

            rect.Width = 30;
            rect.Inflate(-3, -4);

            if (e.IsSelected)
            {
                BasicBlockPainter.RenderFlatBackground(
                    e.Graphics,
                    e.ItemRect,
                    colorBox.XTheme.ListBoxTheme.SelectedItemBackColor,
                    ButtonBorderType.Rectangle);
            }

            Color c = Color.FromKnownColor((KnownColor)colorBox.Items[e.Index]);

            using (SolidBrush sb = new SolidBrush(c))
            {
                e.Graphics.FillRectangle(sb, rect);
            }

            e.Graphics.DrawRectangle(Pens.Black, Gdu.WinFormUI.MyGraphics.RectHelper.DecreaseWH(rect));

            rect = e.ItemRect;
            rect.Offset(30, 0);
            rect.Width -= 30;

            TextRenderer.DrawText(
                e.Graphics,
                colorBox.Items[e.Index].ToString(),
                colorBox.XTheme.ListBoxTheme.TextFont,
                rect,
                (e.IsSelected ? colorBox.XTheme.ListBoxTheme.SelectedItemTextColor :
                colorBox.XTheme.ListBoxTheme.TextColor),
                TextFormatFlags.Left | TextFormatFlags.VerticalCenter |
                TextFormatFlags.PreserveGraphicsClipping);
        }

        private void colorBox_DrawComboText(object sender, DrawComboTextEventArgs e)
        {
            if (e.SelectedValue == null)
                return;
            if (!(e.SelectedValue is KnownColor))
                return;

            KnownColor kc = (KnownColor)e.SelectedValue;

            Color c = Color.FromKnownColor(kc);

            Rectangle rect = e.TextRect;

            rect.Width = 30;
            rect.Inflate(-3, -4);
            BasicBlockPainter.RenderFlatBackground(
                e.Graphics, rect, c, ButtonBorderType.Rectangle);
            BasicBlockPainter.RenderBorder(
                e.Graphics, rect, Color.Black, ButtonBorderType.Rectangle);

            rect = e.TextRect;
            rect.Offset(30, 0);
            rect.Width -= 30;

            TextRenderer.DrawText(
                e.Graphics,
                kc.ToString(),
                colorBox.XTheme.ComboTextFont,
                rect,
                colorBox.XTheme.ComboTextColor,
                TextFormatFlags.Left | TextFormatFlags.VerticalCenter);
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            btnStop.Enabled = true;
            btnStart.Enabled = false;

            gmRollingBar1.StartRolling();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            btnStart.Enabled = true;
            btnStop.Enabled = false;

            gmRollingBar1.StopRolling();
        }

        private void gmButton3_Click(object sender, EventArgs e)
        {
            new FormScrollBar().Show();
        }

        private void gmButton4_Click(object sender, EventArgs e)
        {
            new FormTrackBar().Show();
        }

        private void gmButton2_Click(object sender, EventArgs e)
        {
            new FormProgressBar().Show();
        }

        private void gmButton1_Click(object sender, EventArgs e)
        {
            new FormRollingBar().Show();
        }

        private void gmButton5_Click(object sender, EventArgs e)
        {
            FormButtons f = new FormButtons();
            f.XTheme = this.XTheme;
            f.Show();
        }

        private void gmButton6_Click(object sender, EventArgs e)
        {
            FormListBox f = new FormListBox();
            f.XTheme = this.XTheme;
            f.Show();
        }       
    }
}
