﻿namespace Gmei.ControlDemo
{
    partial class FormProgressBar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gmProgressBar1 = new Gdu.WinFormUI.GMProgressBar();
            this.gmProgressBar2 = new Gdu.WinFormUI.GMProgressBar();
            this.gmProgressBar3 = new Gdu.WinFormUI.GMProgressBar();
            this.gmProgressBar4 = new Gdu.WinFormUI.GMProgressBar();
            this.gmTrackBar1 = new Gdu.WinFormUI.GMTrackBar();
            this.SuspendLayout();
            // 
            // gmProgressBar1
            // 
            this.gmProgressBar1.Location = new System.Drawing.Point(24, 61);
            this.gmProgressBar1.Name = "gmProgressBar1";
            this.gmProgressBar1.Size = new System.Drawing.Size(246, 18);
            this.gmProgressBar1.TabIndex = 0;
            this.gmProgressBar1.TabStop = false;
            this.gmProgressBar1.XTheme = null;
            // 
            // gmProgressBar2
            // 
            this.gmProgressBar2.Location = new System.Drawing.Point(24, 90);
            this.gmProgressBar2.Name = "gmProgressBar2";
            this.gmProgressBar2.Size = new System.Drawing.Size(246, 18);
            this.gmProgressBar2.TabIndex = 1;
            this.gmProgressBar2.TabStop = false;
            this.gmProgressBar2.XTheme = null;
            // 
            // gmProgressBar3
            // 
            this.gmProgressBar3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.gmProgressBar3.Location = new System.Drawing.Point(24, 119);
            this.gmProgressBar3.Name = "gmProgressBar3";
            this.gmProgressBar3.Size = new System.Drawing.Size(246, 22);
            this.gmProgressBar3.TabIndex = 2;
            this.gmProgressBar3.TabStop = false;
            this.gmProgressBar3.XTheme = null;
            // 
            // gmProgressBar4
            // 
            this.gmProgressBar4.Location = new System.Drawing.Point(70, 202);
            this.gmProgressBar4.Name = "gmProgressBar4";
            this.gmProgressBar4.Shape = Gdu.WinFormUI.ProgressBarShapeStyle.Circle;
            this.gmProgressBar4.Size = new System.Drawing.Size(150, 111);
            this.gmProgressBar4.TabIndex = 3;
            this.gmProgressBar4.TabStop = false;
            this.gmProgressBar4.XTheme = null;
            // 
            // gmTrackBar1
            // 
            this.gmTrackBar1.Location = new System.Drawing.Point(24, 161);
            this.gmTrackBar1.Maximum = 100;
            this.gmTrackBar1.Name = "gmTrackBar1";
            this.gmTrackBar1.Size = new System.Drawing.Size(246, 24);
            this.gmTrackBar1.TabIndex = 4;
            this.gmTrackBar1.XTheme = null;
            this.gmTrackBar1.ValueChanged += new System.EventHandler(this.gmTrackBar1_ValueChanged);
            // 
            // FormProgressBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(268, 300);
            this.Controls.Add(this.gmTrackBar1);
            this.Controls.Add(this.gmProgressBar4);
            this.Controls.Add(this.gmProgressBar3);
            this.Controls.Add(this.gmProgressBar2);
            this.Controls.Add(this.gmProgressBar1);
            this.Name = "FormProgressBar";
            this.Text = "GMProgressBar示例";
            this.Load += new System.EventHandler(this.FormProgressBar_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Gdu.WinFormUI.GMProgressBar gmProgressBar1;
        private Gdu.WinFormUI.GMProgressBar gmProgressBar2;
        private Gdu.WinFormUI.GMProgressBar gmProgressBar3;
        private Gdu.WinFormUI.GMProgressBar gmProgressBar4;
        private Gdu.WinFormUI.GMTrackBar gmTrackBar1;
    }
}