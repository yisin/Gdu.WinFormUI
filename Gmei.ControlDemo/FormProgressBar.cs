﻿using System;
using System.Drawing;
using System.Windows.Forms;

using Gdu.WinFormUI;

namespace Gmei.ControlDemo
{
    public partial class FormProgressBar : GMForm
    {
        public FormProgressBar()
        {
            InitializeComponent();
        }

        private void FormProgressBar_Load(object sender, EventArgs e)
        {
            base.XTheme = new ThemeFormDevExpress();

            gmProgressBar1.XTheme = new ThemeProgressBarGreen();
            gmProgressBar2.XTheme = new ThemeProgressBarBlue();
            gmProgressBar4.XTheme = new ThemeProgressBarSoftGreen();
        }

        private void gmTrackBar1_ValueChanged(object sender, EventArgs e)
        {
            gmProgressBar1.Percentage = gmTrackBar1.Value;
            gmProgressBar2.Percentage = gmTrackBar1.Value;
            gmProgressBar3.Percentage = gmTrackBar1.Value;
            gmProgressBar4.Percentage = gmTrackBar1.Value;
        }
    }
}
