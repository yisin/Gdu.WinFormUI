﻿namespace Gmei.ControlDemo
{
    partial class FormRollingBar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.gmRollingBar1 = new Gdu.WinFormUI.GMRollingBar();
            this.gmRollingBar2 = new Gdu.WinFormUI.GMRollingBar();
            this.gmRollingBar3 = new Gdu.WinFormUI.GMRollingBar();
            this.gmRollingBar4 = new Gdu.WinFormUI.GMRollingBar();
            this.gmRollingBar5 = new Gdu.WinFormUI.GMRollingBar();
            this.gmRollingBar6 = new Gdu.WinFormUI.GMRollingBar();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(28, 59);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(108, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Start Rolling";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(142, 59);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(109, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Stop Rolling";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // gmRollingBar1
            // 
            this.gmRollingBar1.Location = new System.Drawing.Point(35, 102);
            this.gmRollingBar1.Name = "gmRollingBar1";
            this.gmRollingBar1.RefleshFrequency = 120;
            this.gmRollingBar1.Size = new System.Drawing.Size(64, 64);
            this.gmRollingBar1.TabIndex = 2;
            this.gmRollingBar1.TabStop = false;
            this.gmRollingBar1.XTheme = null;
            // 
            // gmRollingBar2
            // 
            this.gmRollingBar2.Location = new System.Drawing.Point(126, 102);
            this.gmRollingBar2.Name = "gmRollingBar2";
            this.gmRollingBar2.RefleshFrequency = 30;
            this.gmRollingBar2.Size = new System.Drawing.Size(64, 64);
            this.gmRollingBar2.TabIndex = 3;
            this.gmRollingBar2.TabStop = false;
            this.gmRollingBar2.XTheme = null;
            // 
            // gmRollingBar3
            // 
            this.gmRollingBar3.Location = new System.Drawing.Point(218, 102);
            this.gmRollingBar3.Name = "gmRollingBar3";
            this.gmRollingBar3.RefleshFrequency = 90;
            this.gmRollingBar3.Size = new System.Drawing.Size(64, 64);
            this.gmRollingBar3.TabIndex = 4;
            this.gmRollingBar3.TabStop = false;
            this.gmRollingBar3.XTheme = null;
            // 
            // gmRollingBar4
            // 
            this.gmRollingBar4.Location = new System.Drawing.Point(35, 183);
            this.gmRollingBar4.Name = "gmRollingBar4";
            this.gmRollingBar4.RefleshFrequency = 120;
            this.gmRollingBar4.Size = new System.Drawing.Size(64, 64);
            this.gmRollingBar4.Style = Gdu.WinFormUI.RollingBarStyle.ChromeOneQuarter;
            this.gmRollingBar4.TabIndex = 5;
            this.gmRollingBar4.TabStop = false;
            this.gmRollingBar4.XTheme = null;
            // 
            // gmRollingBar5
            // 
            this.gmRollingBar5.Location = new System.Drawing.Point(126, 183);
            this.gmRollingBar5.Name = "gmRollingBar5";
            this.gmRollingBar5.RefleshFrequency = 120;
            this.gmRollingBar5.Size = new System.Drawing.Size(64, 64);
            this.gmRollingBar5.Style = Gdu.WinFormUI.RollingBarStyle.DiamondRing;
            this.gmRollingBar5.TabIndex = 6;
            this.gmRollingBar5.TabStop = false;
            this.gmRollingBar5.XTheme = null;
            // 
            // gmRollingBar6
            // 
            this.gmRollingBar6.Location = new System.Drawing.Point(218, 183);
            this.gmRollingBar6.Name = "gmRollingBar6";
            this.gmRollingBar6.RefleshFrequency = 110;
            this.gmRollingBar6.Size = new System.Drawing.Size(64, 64);
            this.gmRollingBar6.Style = Gdu.WinFormUI.RollingBarStyle.BigGuyLeadsLittleGuys;
            this.gmRollingBar6.TabIndex = 7;
            this.gmRollingBar6.TabStop = false;
            this.gmRollingBar6.XTheme = null;
            // 
            // FormRollingBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 230);
            this.Controls.Add(this.gmRollingBar6);
            this.Controls.Add(this.gmRollingBar5);
            this.Controls.Add(this.gmRollingBar4);
            this.Controls.Add(this.gmRollingBar3);
            this.Controls.Add(this.gmRollingBar2);
            this.Controls.Add(this.gmRollingBar1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "FormRollingBar";
            this.Text = "GMRollingBar示例";
            this.Load += new System.EventHandler(this.FormRollingBar_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private Gdu.WinFormUI.GMRollingBar gmRollingBar1;
        private Gdu.WinFormUI.GMRollingBar gmRollingBar2;
        private Gdu.WinFormUI.GMRollingBar gmRollingBar3;
        private Gdu.WinFormUI.GMRollingBar gmRollingBar4;
        private Gdu.WinFormUI.GMRollingBar gmRollingBar5;
        private Gdu.WinFormUI.GMRollingBar gmRollingBar6;
    }
}