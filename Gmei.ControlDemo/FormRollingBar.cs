﻿using System;
using System.Drawing;
using System.Windows.Forms;

using Gdu.WinFormUI;

namespace Gmei.ControlDemo
{
    public partial class FormRollingBar : GMForm
    {
        public FormRollingBar()
        {
            InitializeComponent();
        }

        private void FormRollingBar_Load(object sender, EventArgs e)
        {
            base.XTheme = new ThemeFormDevExpress();

            gmRollingBar2.XTheme = new ThemeRollingBarFullCircle();
            gmRollingBar3.XTheme = new ThemeRollingBarCircleDots();

            gmRollingBar4.XTheme = new ThemeRollingBarChromeGreen();
            gmRollingBar5.XTheme = new ThemeRollingBarRing();
            gmRollingBar6.XTheme = new ThemeRollingBarGuys();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            button2.Enabled = true;

            gmRollingBar1.StartRolling();
            gmRollingBar2.StartRolling();
            gmRollingBar3.StartRolling();
            gmRollingBar4.StartRolling();
            gmRollingBar5.StartRolling();
            gmRollingBar6.StartRolling();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            button2.Enabled = false;
            button1.Enabled = true;

            gmRollingBar1.StopRolling();
            gmRollingBar2.StopRolling();
            gmRollingBar3.StopRolling();
            gmRollingBar4.StopRolling();
            gmRollingBar5.StopRolling();
            gmRollingBar6.StopRolling();
        }
    }
}
