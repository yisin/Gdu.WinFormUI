﻿namespace Gmei.ControlDemo
{
    partial class FormScrollBar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gmhScrollBar1 = new Gdu.WinFormUI.GMHScrollBar();
            this.gmhScrollBar2 = new Gdu.WinFormUI.GMHScrollBar();
            this.gmhScrollBar3 = new Gdu.WinFormUI.GMHScrollBar();
            this.gmhScrollBar4 = new Gdu.WinFormUI.GMHScrollBar();
            this.gmhScrollBar5 = new Gdu.WinFormUI.GMHScrollBar();
            this.gmhScrollBar6 = new Gdu.WinFormUI.GMHScrollBar();
            this.gmvScrollBar1 = new Gdu.WinFormUI.GMVScrollBar();
            this.gmvScrollBar2 = new Gdu.WinFormUI.GMVScrollBar();
            this.gmvScrollBar3 = new Gdu.WinFormUI.GMVScrollBar();
            this.gmvScrollBar4 = new Gdu.WinFormUI.GMVScrollBar();
            this.gmvScrollBar5 = new Gdu.WinFormUI.GMVScrollBar();
            this.gmvScrollBar6 = new Gdu.WinFormUI.GMVScrollBar();
            this.gmvScrollBar7 = new Gdu.WinFormUI.GMVScrollBar();
            this.gmhScrollBar7 = new Gdu.WinFormUI.GMHScrollBar();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // gmhScrollBar1
            // 
            this.gmhScrollBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gmhScrollBar1.LargeChange = 10;
            this.gmhScrollBar1.Location = new System.Drawing.Point(20, 49);
            this.gmhScrollBar1.MiddleButtonLengthPercentage = 10;
            this.gmhScrollBar1.Name = "gmhScrollBar1";
            this.gmhScrollBar1.Size = new System.Drawing.Size(431, 19);
            this.gmhScrollBar1.SmallChange = 1;
            this.gmhScrollBar1.TabIndex = 0;
            this.gmhScrollBar1.TabStop = false;
            this.gmhScrollBar1.ValueChanged += new System.EventHandler(this.gmhScrollBar1_ValueChanged);
            // 
            // gmhScrollBar2
            // 
            this.gmhScrollBar2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gmhScrollBar2.LargeChange = 10;
            this.gmhScrollBar2.Location = new System.Drawing.Point(18, 77);
            this.gmhScrollBar2.MiddleButtonLengthPercentage = 10;
            this.gmhScrollBar2.Name = "gmhScrollBar2";
            this.gmhScrollBar2.Size = new System.Drawing.Size(432, 16);
            this.gmhScrollBar2.SmallChange = 1;
            this.gmhScrollBar2.TabIndex = 1;
            this.gmhScrollBar2.TabStop = false;
            this.gmhScrollBar2.ValueChanged += new System.EventHandler(this.gmhScrollBar1_ValueChanged);
            // 
            // gmhScrollBar3
            // 
            this.gmhScrollBar3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gmhScrollBar3.LargeChange = 10;
            this.gmhScrollBar3.Location = new System.Drawing.Point(22, 103);
            this.gmhScrollBar3.MiddleButtonLengthPercentage = 10;
            this.gmhScrollBar3.Name = "gmhScrollBar3";
            this.gmhScrollBar3.Size = new System.Drawing.Size(427, 16);
            this.gmhScrollBar3.SmallChange = 1;
            this.gmhScrollBar3.TabIndex = 2;
            this.gmhScrollBar3.TabStop = false;
            this.gmhScrollBar3.ValueChanged += new System.EventHandler(this.gmhScrollBar1_ValueChanged);
            // 
            // gmhScrollBar4
            // 
            this.gmhScrollBar4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gmhScrollBar4.LargeChange = 10;
            this.gmhScrollBar4.Location = new System.Drawing.Point(21, 127);
            this.gmhScrollBar4.MiddleButtonLengthPercentage = 10;
            this.gmhScrollBar4.Name = "gmhScrollBar4";
            this.gmhScrollBar4.Size = new System.Drawing.Size(427, 21);
            this.gmhScrollBar4.SmallChange = 1;
            this.gmhScrollBar4.TabIndex = 3;
            this.gmhScrollBar4.TabStop = false;
            this.gmhScrollBar4.ValueChanged += new System.EventHandler(this.gmhScrollBar1_ValueChanged);
            // 
            // gmhScrollBar5
            // 
            this.gmhScrollBar5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gmhScrollBar5.LargeChange = 10;
            this.gmhScrollBar5.Location = new System.Drawing.Point(23, 155);
            this.gmhScrollBar5.MiddleButtonLengthPercentage = 10;
            this.gmhScrollBar5.Name = "gmhScrollBar5";
            this.gmhScrollBar5.Size = new System.Drawing.Size(424, 20);
            this.gmhScrollBar5.SmallChange = 1;
            this.gmhScrollBar5.TabIndex = 4;
            this.gmhScrollBar5.TabStop = false;
            this.gmhScrollBar5.ValueChanged += new System.EventHandler(this.gmhScrollBar1_ValueChanged);
            // 
            // gmhScrollBar6
            // 
            this.gmhScrollBar6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gmhScrollBar6.LargeChange = 10;
            this.gmhScrollBar6.Location = new System.Drawing.Point(24, 181);
            this.gmhScrollBar6.MiddleButtonLengthPercentage = 10;
            this.gmhScrollBar6.Name = "gmhScrollBar6";
            this.gmhScrollBar6.Size = new System.Drawing.Size(422, 22);
            this.gmhScrollBar6.SmallChange = 1;
            this.gmhScrollBar6.TabIndex = 5;
            this.gmhScrollBar6.TabStop = false;
            this.gmhScrollBar6.ValueChanged += new System.EventHandler(this.gmhScrollBar1_ValueChanged);
            // 
            // gmvScrollBar1
            // 
            this.gmvScrollBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.gmvScrollBar1.LargeChange = 10;
            this.gmvScrollBar1.Location = new System.Drawing.Point(33, 240);
            this.gmvScrollBar1.MiddleButtonLengthPercentage = 10;
            this.gmvScrollBar1.Name = "gmvScrollBar1";
            this.gmvScrollBar1.Size = new System.Drawing.Size(19, 221);
            this.gmvScrollBar1.SmallChange = 1;
            this.gmvScrollBar1.TabIndex = 6;
            this.gmvScrollBar1.TabStop = false;
            this.gmvScrollBar1.ValueChanged += new System.EventHandler(this.gmhScrollBar1_ValueChanged);
            // 
            // gmvScrollBar2
            // 
            this.gmvScrollBar2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.gmvScrollBar2.LargeChange = 10;
            this.gmvScrollBar2.Location = new System.Drawing.Point(74, 240);
            this.gmvScrollBar2.MiddleButtonLengthPercentage = 10;
            this.gmvScrollBar2.Name = "gmvScrollBar2";
            this.gmvScrollBar2.Size = new System.Drawing.Size(18, 220);
            this.gmvScrollBar2.SmallChange = 1;
            this.gmvScrollBar2.TabIndex = 7;
            this.gmvScrollBar2.TabStop = false;
            this.gmvScrollBar2.ValueChanged += new System.EventHandler(this.gmhScrollBar1_ValueChanged);
            // 
            // gmvScrollBar3
            // 
            this.gmvScrollBar3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.gmvScrollBar3.LargeChange = 10;
            this.gmvScrollBar3.Location = new System.Drawing.Point(121, 240);
            this.gmvScrollBar3.MiddleButtonLengthPercentage = 10;
            this.gmvScrollBar3.Name = "gmvScrollBar3";
            this.gmvScrollBar3.Size = new System.Drawing.Size(19, 220);
            this.gmvScrollBar3.SmallChange = 1;
            this.gmvScrollBar3.TabIndex = 8;
            this.gmvScrollBar3.TabStop = false;
            this.gmvScrollBar3.ValueChanged += new System.EventHandler(this.gmhScrollBar1_ValueChanged);
            // 
            // gmvScrollBar4
            // 
            this.gmvScrollBar4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.gmvScrollBar4.LargeChange = 10;
            this.gmvScrollBar4.Location = new System.Drawing.Point(170, 240);
            this.gmvScrollBar4.MiddleButtonLengthPercentage = 10;
            this.gmvScrollBar4.Name = "gmvScrollBar4";
            this.gmvScrollBar4.Size = new System.Drawing.Size(19, 220);
            this.gmvScrollBar4.SmallChange = 1;
            this.gmvScrollBar4.TabIndex = 9;
            this.gmvScrollBar4.TabStop = false;
            this.gmvScrollBar4.ValueChanged += new System.EventHandler(this.gmhScrollBar1_ValueChanged);
            // 
            // gmvScrollBar5
            // 
            this.gmvScrollBar5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.gmvScrollBar5.LargeChange = 10;
            this.gmvScrollBar5.Location = new System.Drawing.Point(217, 240);
            this.gmvScrollBar5.MiddleButtonLengthPercentage = 10;
            this.gmvScrollBar5.Name = "gmvScrollBar5";
            this.gmvScrollBar5.Size = new System.Drawing.Size(18, 220);
            this.gmvScrollBar5.SmallChange = 1;
            this.gmvScrollBar5.TabIndex = 10;
            this.gmvScrollBar5.TabStop = false;
            this.gmvScrollBar5.ValueChanged += new System.EventHandler(this.gmhScrollBar1_ValueChanged);
            // 
            // gmvScrollBar6
            // 
            this.gmvScrollBar6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.gmvScrollBar6.LargeChange = 10;
            this.gmvScrollBar6.Location = new System.Drawing.Point(261, 240);
            this.gmvScrollBar6.MiddleButtonLengthPercentage = 10;
            this.gmvScrollBar6.Name = "gmvScrollBar6";
            this.gmvScrollBar6.Size = new System.Drawing.Size(19, 219);
            this.gmvScrollBar6.SmallChange = 1;
            this.gmvScrollBar6.TabIndex = 11;
            this.gmvScrollBar6.TabStop = false;
            this.gmvScrollBar6.ValueChanged += new System.EventHandler(this.gmhScrollBar1_ValueChanged);
            // 
            // gmvScrollBar7
            // 
            this.gmvScrollBar7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.gmvScrollBar7.LargeChange = 10;
            this.gmvScrollBar7.Location = new System.Drawing.Point(302, 240);
            this.gmvScrollBar7.MiddleButtonLengthPercentage = 10;
            this.gmvScrollBar7.Name = "gmvScrollBar7";
            this.gmvScrollBar7.Size = new System.Drawing.Size(18, 220);
            this.gmvScrollBar7.SmallChange = 1;
            this.gmvScrollBar7.TabIndex = 12;
            this.gmvScrollBar7.TabStop = false;
            this.gmvScrollBar7.ValueChanged += new System.EventHandler(this.gmhScrollBar1_ValueChanged);
            // 
            // gmhScrollBar7
            // 
            this.gmhScrollBar7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gmhScrollBar7.LargeChange = 10;
            this.gmhScrollBar7.Location = new System.Drawing.Point(26, 209);
            this.gmhScrollBar7.MiddleButtonLengthPercentage = 10;
            this.gmhScrollBar7.Name = "gmhScrollBar7";
            this.gmhScrollBar7.Size = new System.Drawing.Size(419, 21);
            this.gmhScrollBar7.SmallChange = 1;
            this.gmhScrollBar7.TabIndex = 13;
            this.gmhScrollBar7.TabStop = false;
            this.gmhScrollBar7.ValueChanged += new System.EventHandler(this.gmhScrollBar1_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(375, 282);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 14;
            this.label1.Text = "Value:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(422, 282);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(11, 12);
            this.label2.TabIndex = 15;
            this.label2.Text = "0";
            // 
            // FormScrollBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(473, 477);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gmhScrollBar7);
            this.Controls.Add(this.gmvScrollBar7);
            this.Controls.Add(this.gmvScrollBar6);
            this.Controls.Add(this.gmvScrollBar5);
            this.Controls.Add(this.gmvScrollBar4);
            this.Controls.Add(this.gmvScrollBar3);
            this.Controls.Add(this.gmvScrollBar2);
            this.Controls.Add(this.gmvScrollBar1);
            this.Controls.Add(this.gmhScrollBar6);
            this.Controls.Add(this.gmhScrollBar5);
            this.Controls.Add(this.gmhScrollBar4);
            this.Controls.Add(this.gmhScrollBar3);
            this.Controls.Add(this.gmhScrollBar2);
            this.Controls.Add(this.gmhScrollBar1);
            this.Name = "FormScrollBar";
            this.Text = "GMScrollBar示例";
            this.Load += new System.EventHandler(this.FormScrollBar_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Gdu.WinFormUI.GMHScrollBar gmhScrollBar1;
        private Gdu.WinFormUI.GMHScrollBar gmhScrollBar2;
        private Gdu.WinFormUI.GMHScrollBar gmhScrollBar3;
        private Gdu.WinFormUI.GMHScrollBar gmhScrollBar4;
        private Gdu.WinFormUI.GMHScrollBar gmhScrollBar5;
        private Gdu.WinFormUI.GMHScrollBar gmhScrollBar6;
        private Gdu.WinFormUI.GMVScrollBar gmvScrollBar1;
        private Gdu.WinFormUI.GMVScrollBar gmvScrollBar2;
        private Gdu.WinFormUI.GMVScrollBar gmvScrollBar3;
        private Gdu.WinFormUI.GMVScrollBar gmvScrollBar4;
        private Gdu.WinFormUI.GMVScrollBar gmvScrollBar5;
        private Gdu.WinFormUI.GMVScrollBar gmvScrollBar6;
        private Gdu.WinFormUI.GMVScrollBar gmvScrollBar7;
        private Gdu.WinFormUI.GMHScrollBar gmhScrollBar7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;

    }
}