﻿using System;
using System.Drawing;
using System.Windows.Forms;

using Gdu.WinFormUI;

namespace Gmei.ControlDemo
{
    public partial class FormScrollBar : GMForm
    {
        public FormScrollBar()
        {
            InitializeComponent();
        }

        private void FormScrollBar_Load(object sender, EventArgs e)
        {
            base.XTheme = new ThemeFormDevExpress();

            gmhScrollBar1.Height = 15;
            gmvScrollBar1.Width = 15;

            gmhScrollBar2.SetNewTheme(new ThemeScrollbarChrome());
            gmhScrollBar2.Height = gmhScrollBar2.XTheme.BestUndirectLen;
            gmvScrollBar2.SetNewTheme(gmhScrollBar2.XTheme);
            gmvScrollBar2.Width = gmhScrollBar2.XTheme.BestUndirectLen;

            gmhScrollBar3.SetNewTheme(new  ThemeScrollbarDevStyle());
            gmhScrollBar3.Height = gmhScrollBar3.XTheme.BestUndirectLen;
            gmvScrollBar3.SetNewTheme(gmhScrollBar3.XTheme);
            gmvScrollBar3.Width = gmhScrollBar3.XTheme.BestUndirectLen;

            gmhScrollBar4.SetNewTheme(new  ThemeScrollbarVS2013());
            gmhScrollBar4.Height = gmhScrollBar4.XTheme.BestUndirectLen;
            gmvScrollBar4.SetNewTheme(gmhScrollBar4.XTheme);
            gmvScrollBar4.Width = gmhScrollBar4.XTheme.BestUndirectLen;

            gmhScrollBar5.SetNewTheme(new  ThemeScrollbarXMPBlack());
            gmhScrollBar5.Height = gmhScrollBar5.XTheme.BestUndirectLen;
            gmvScrollBar5.SetNewTheme(gmhScrollBar5.XTheme);
            gmvScrollBar5.Width = gmhScrollBar5.XTheme.BestUndirectLen;

            gmhScrollBar6.SetNewTheme(new  ThemeScrollbarNoSideButtons());
            gmhScrollBar6.Height = gmhScrollBar6.XTheme.BestUndirectLen;
            gmvScrollBar6.SetNewTheme(gmhScrollBar6.XTheme);
            gmvScrollBar6.Width = gmhScrollBar6.XTheme.BestUndirectLen;

            gmhScrollBar7.SetNewTheme(new  ThemeScrollbarEllipse());
            gmhScrollBar7.Height = gmhScrollBar7.XTheme.BestUndirectLen;
            gmvScrollBar7.SetNewTheme(gmhScrollBar7.XTheme);
            gmvScrollBar7.Width = gmhScrollBar7.XTheme.BestUndirectLen;
        }

        private void gmhScrollBar1_ValueChanged(object sender, EventArgs e)
        {
            WLScrollBar obj = sender as WLScrollBar;
            if (obj != null)
            {
                label2.Text = obj.Value.ToString();
            }
        }
    }
}
