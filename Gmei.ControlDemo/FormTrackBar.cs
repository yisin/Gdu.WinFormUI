﻿using System;
using System.Drawing;
using System.Windows.Forms;

using Gdu.WinFormUI;

namespace Gmei.ControlDemo
{
    public partial class FormTrackBar : GMForm
    {
        public FormTrackBar()
        {
            InitializeComponent();
        }

        private void FormTrackBar_Load(object sender, EventArgs e)
        {
            base.XTheme = new ThemeFormDevExpress();

            comboBox1.Items.Add("default");
            comboBox1.Items.Add("TTPlayer");
            comboBox1.Items.Add("XMPBlack");
            comboBox1.Items.Add("Metropolis");

            comboBox2.Items.Add("None");
            comboBox2.Items.Add("Both");
            comboBox2.Items.Add("TopLeft");
            comboBox2.Items.Add("BottomRight");
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            GMTrackBarThemeBase theme = null;
            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    break;
                case 1:
                    theme = new ThemeTrackBarTTPlayer();
                    break;
                case 2:
                    theme = new ThemeTrackBarXMP();
                    break;
                case 3:
                    theme = new ThemeTrackBarMetropolis();
                    break;
            }
            gmTrackBar1.XTheme = theme;
            gmTrackBar2.XTheme = theme;
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBox2.SelectedIndex)
            {
                case 0:
                    gmTrackBar1.TickSide = TickStyle.None;
                    break;
                case 1:
                    gmTrackBar1.TickSide = TickStyle.Both;
                    break;
                case 2:
                    gmTrackBar1.TickSide = TickStyle.TopLeft;
                    break;
                case 3:
                    gmTrackBar1.TickSide = TickStyle.BottomRight;
                    break;
            }
            gmTrackBar2.TickSide = gmTrackBar1.TickSide;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            gmTrackBar1.Minimum = (int)numericUpDown1.Value;
            gmTrackBar2.Minimum = (int)numericUpDown1.Value;
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            gmTrackBar1.Maximum = (int)numericUpDown2.Value;
            gmTrackBar2.Maximum = (int)numericUpDown2.Value;
        }

        private void numericUpDown3_ValueChanged(object sender, EventArgs e)
        {
            gmTrackBar1.TickFrequency = (int)numericUpDown3.Value;
            gmTrackBar2.TickFrequency = (int)numericUpDown3.Value;
        }

        private void numericUpDown4_ValueChanged(object sender, EventArgs e)
        {
            gmTrackBar1.Value = (int)numericUpDown4.Value;
            gmTrackBar2.Value = (int)numericUpDown4.Value;
        }

        private void gmTrackBar1_ValueChanged(object sender, EventArgs e)
        {
            numericUpDown4.Value = gmTrackBar1.Value;
        }

        private void gmTrackBar2_ValueChanged(object sender, EventArgs e)
        {
            numericUpDown4.Value = gmTrackBar2.Value;
        }
    }
}
