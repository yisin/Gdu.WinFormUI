﻿using System;
using System.Windows.Forms;

namespace Gmei.MdiDemo
{
    public partial class frmAbout : Gdu.WinFormUI.GMForm
    {
        public frmAbout()
        {
            InitializeComponent();
            base.XTheme = new ChromeTheme();
            base.Resizable = false;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.CaptionHeight = 24;
            base.XTheme.DrawCaptionIcon = true;
            base.XTheme.DrawCaptionText = true;
            base.XTheme.FormBorderInmostColor = System.Drawing.Color.Transparent;
            base.XTheme.CaptionTextColor =
                Gdu.WinFormUI.MyGraphics.ColorHelper.GetDarkerColor(System.Drawing.Color.White, 6);
            base.ShowInTaskbar = false;

            panel1.BackColor = System.Drawing.Color.FromArgb(248, 248, 248);
            panel1.Dock = DockStyle.Fill;
        }

        protected override void OnKeyUp(KeyEventArgs e)
        {
            base.OnKeyUp(e);
            if (e.KeyData == Keys.Escape)
                base.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            base.Close();
        }
    }
}
