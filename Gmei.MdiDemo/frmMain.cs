﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;

namespace Gmei.MdiDemo
{
    public partial class frmMain : Gdu.WinFormUI.GMForm
    {        
        MdiClient _mdiClient;
        StopButtonFunction _stopBtnFunction;
        string _homePage = "http://www.mtime.com";
        List<string> _listStringViewHistory;
        List<ToolStripMenuItem> _listMenuViewHistory;

        private int PanelTopHeight
        { get { return 36; } }

        public frmMain()
        {
            InitializeComponent();

            _listStringViewHistory = new List<string>();
            _listMenuViewHistory = new List<ToolStripMenuItem>();
            mnItemClearView.Enabled = false;
            mnStoneHistory.Visible = false;

            _mdiClient = FindMdiClient();
            _stopBtnFunction = StopButtonFunction.Reflesh;
        }

        private MdiClient FindMdiClient()
        {
            MdiClient mdi = null;
            foreach (Control c in Controls)
            {
                if ((mdi = c as MdiClient) != null)
                {
                    break;
                }
            }
            return mdi;
        }

        public void CreateNewHistoryItem(string url, string title)
        {
            // 把#后面的内容去掉
            int pos = url.IndexOf('#');
            if (pos > 0)
                url = url.Substring(0, pos);

            if (_listStringViewHistory.Contains(url))            
                return;            

            ToolStripMenuItem tmp = new ToolStripMenuItem();
            tmp.Text = title;
            tmp.Tag = url;
            tmp.Click+=new EventHandler(MenuHistoryItem_Click);
            _listMenuViewHistory.Add(tmp);
            mnItemRecentlyView.DropDownItems.Add(tmp);
            _listStringViewHistory.Add(url);
            mnItemClearView.Enabled = true;
            mnStoneHistory.Visible = true;
        }

        private void ClearViewHistory()
        {
            foreach (ToolStripMenuItem item in _listMenuViewHistory)
            {
                mnItemRecentlyView.DropDownItems.Remove(item);
                item.Dispose();
            }
            _listMenuViewHistory = new List<ToolStripMenuItem>();
            mnItemClearView.Enabled = false;
            mnStoneHistory.Visible = false;
            _listStringViewHistory = new List<string>();
        }

        private void MenuHistoryItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            string url = item.Tag as string;
            GoNewWebSite(url, true);
        }

        #region mdi-bar event handler

        private void NewFormClick(object sender, EventArgs e)
        {
            CreateNewChild();
        }

        private void ChildFormClose(object sender, EventArgs e)
        {
            Form f = sender as Form;
            if (f != null)
                f.Close();
        }

        private void frmMain_MdiBarCreated(object sender, EventArgs e)
        {
            SetFormItemLocation();
                        
            CreateNewChild();
        }

        #endregion

        public void SetTxtURL(string url)
        {
            txtURL.Text = url;
        }

        public void SetButtons(bool canBack, bool canForward, bool canStop)
        {
            btnBack.Enabled = canBack;
            if (canBack)
                btnBack.BackgroundImage = Properties.Resources.back;
            else
                btnBack.BackgroundImage = Properties.Resources.back_disable;

            btnForward.Enabled = canForward;
            if (canForward)
                btnForward.BackgroundImage = Properties.Resources.forward;
            else
                btnForward.BackgroundImage = Properties.Resources.forward_disable;

            if (canStop)
            {
                _stopBtnFunction = StopButtonFunction.Stop;
                btnStopOrReflesh.BackgroundImage = Properties.Resources.stop;
            }
            else
            {
                _stopBtnFunction = StopButtonFunction.Reflesh;
                btnStopOrReflesh.BackgroundImage = Properties.Resources.reflesh;
            }
        }

        private void CreateNewChild()
        {
            frmChild f = new frmChild();            
            f.FormClosed += new FormClosedEventHandler(base.MdiChildClosed);
            f.VisibleChanged += new EventHandler(base.MdiChildVisibleChange);

            f.Text = "空白页";            
            f.MdiParent = this;
            //f.BackColor = Color.White;
            f.CurrentURL = "about:blank";
            f.WindowState = FormWindowState.Maximized;            
            f.Show();
        }

        public void GoNewWebSite(string url, bool newForm)
        {
            if (newForm)
                CreateNewChild();
            GoNewWebSite(url);
        }

        private void GoNewWebSite(string url)
        {
            frmChild activeForm = ActiveMdiChild as frmChild;
            if (activeForm != null)
            {
                url = url.Trim();
                if (url.ToLower().Equals("about:blank"))
                    return;

                if (!(url.ToLower().StartsWith("http://") || url.ToLower().StartsWith("https://")))
                {
                    url = "http://" + url;
                }
                txtURL.Text = url;
                activeForm.CurrentURL = url;
                activeForm.Navigate(txtURL.Text);
            }
        }

        private void SetMdiBarLocation()
        {
            Padding newMargin = Padding.Empty;
            if (WindowState == FormWindowState.Maximized)
                newMargin = new Padding(BorderWidth, BorderWidth, BorderWidth, 0);
            else
                newMargin = new Padding(BorderWidth, BorderWidth + 14, BorderWidth, 0);
            if (base.XTheme.Mdi_BarMargin != newMargin)
            {
                base.XTheme.Mdi_BarMargin = newMargin;
                Invalidate();
            }
        }

        private void SetPanelTopLocation()
        {
            Point newLocation = new Point( base.MdiBarBounds.Left,base.MdiBarBounds.Bottom);
            if (panelTop.Location == newLocation)
                return;

            var size = new Size(base.MdiBarBounds.Width, PanelTopHeight);
            panelTop.Anchor = AnchorStyles.None;
            panelTop.Location = newLocation;
            panelTop.Size = size;
            panelTop.Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
        }

        private void SetMdiClientLocation()
        {
            if (_mdiClient == null)
                return;

            Point newLocation = new Point(panelTop.Left, panelTop.Bottom);
            if (_mdiClient.Location == newLocation)
                return;

            int height = base.ClientSize.Height - newLocation.Y - 
                (base.ClientSize.Width - panelTop.Width) / 2;

            var size = new Size(panelTop.Width, height);

            _mdiClient.Anchor = AnchorStyles.None;
            _mdiClient.Location = newLocation;
            _mdiClient.Size = size;
            _mdiClient.Anchor =
                AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Bottom;
        }

        private void SetFormItemLocation()
        {
            SetMdiBarLocation();
            SetPanelTopLocation();
            SetMdiClientLocation();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            base.XTheme = new ChromeTheme();

            panelTop.BackColor = base.XTheme.Mdi_BarBottomRegionBackColor;

            base.MdiNewTabButtonClick += new EventHandler(NewFormClick);
            base.MdiTabCloseButtonClick += new EventHandler(ChildFormClose);

            if (_mdiClient != null)
            {
                _mdiClient.Dock = DockStyle.None;
                _mdiClient.BackColor = Color.White;
            }
        }

        private void frmMain_Resize(object sender, EventArgs e)
        {
            if (base.WindowState == FormWindowState.Maximized)
            {
                base.ControlBoxOffset = new Point(4, base.BorderWidth);
            }
            else
            {
                base.ControlBoxOffset = new Point(4, 1);
            }                        
            SetFormItemLocation();
        }

        private void panelTop_Paint(object sender, PaintEventArgs e)
        {
            Point p1 = new Point(0, panelTop.Height - 1);
            Point p2 = new Point(panelTop.Width - 1, panelTop.Height - 1);
            Color c1 = Color.FromArgb(170, 170, 170);
            using (Pen pen = new Pen(c1))
            {
                e.Graphics.DrawLine(pen, p1, p2);
            }
        }

        private void txtURL_LeftImageClick(object sender, EventArgs e)
        {
            MessageBox.Show("you click the left image...");
        }

        private void txtURL_RightImageClick(object sender, EventArgs e)
        {
            MessageBox.Show("you click the right image...");
        }

        private void txtURL_TextKeyEnter(object sender, EventArgs e)
        {
            //MessageBox.Show("you just hit the enter key...");            
            GoNewWebSite(txtURL.Text);
        }        

        private void btnMenu_Click(object sender, EventArgs e)
        {
            menuMain.Show(btnMenu, 0, btnMenu.Height + 2);
        }

        private void frmMain_MdiChildActivate(object sender, EventArgs e)
        {
            // 如果当前没有子窗体了，则退出程序
            if (base.GetCurrentMdiChildren().Count == 0)
                base.Close();
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            GoNewWebSite(_homePage);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild != null)
                (ActiveMdiChild as frmChild).WebOperation(WebOperationType.GoBack);
        }

        private void btnForward_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild != null)
                (ActiveMdiChild as frmChild).WebOperation(WebOperationType.GoForward);
        }

        private void btnStopOrReflesh_Click(object sender, EventArgs e)
        {
            WebOperationType type;
            if (_stopBtnFunction == StopButtonFunction.Stop)
                type = WebOperationType.Stop;
            else
                type = WebOperationType.Reflesh;
            if (ActiveMdiChild != null)
                (ActiveMdiChild as frmChild).WebOperation(type);

        }

        private void mnItemNewTab_Click(object sender, EventArgs e)
        {
            CreateNewChild();
        }

        private void mnItemGoHome_Click(object sender, EventArgs e)
        {
            GoNewWebSite(_homePage);
        }

        private void mnItemClearView_Click(object sender, EventArgs e)
        {
            ClearViewHistory();
        }

        private void mnItemAbout_Click(object sender, EventArgs e)
        {
            frmAbout form = new frmAbout();            
            form.Size = new System.Drawing.Size(374, 210);
            form.ShowDialog();
        }
    }

    internal enum StopButtonFunction
    {
        Stop,
        Reflesh,
    }
}
