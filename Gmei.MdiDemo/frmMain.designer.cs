﻿namespace Gmei.MdiDemo
{
    partial class frmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelTop = new System.Windows.Forms.Panel();
            this.txtURL = new Gmei.MdiDemo.GMText();
            this.btnMenu = new System.Windows.Forms.Button();
            this.btnHome = new System.Windows.Forms.Button();
            this.btnStopOrReflesh = new System.Windows.Forms.Button();
            this.btnForward = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.menuMain = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnItemNewTab = new System.Windows.Forms.ToolStripMenuItem();
            this.mnItemGoHome = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnItemRecentlyView = new System.Windows.Forms.ToolStripMenuItem();
            this.mnItemClearView = new System.Windows.Forms.ToolStripMenuItem();
            this.mnStoneHistory = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnItemAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.panelTop.SuspendLayout();
            this.menuMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTop
            // 
            this.panelTop.Controls.Add(this.txtURL);
            this.panelTop.Controls.Add(this.btnMenu);
            this.panelTop.Controls.Add(this.btnHome);
            this.panelTop.Controls.Add(this.btnStopOrReflesh);
            this.panelTop.Controls.Add(this.btnForward);
            this.panelTop.Controls.Add(this.btnBack);
            this.panelTop.Location = new System.Drawing.Point(27, 51);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(403, 49);
            this.panelTop.TabIndex = 1;
            this.panelTop.Paint += new System.Windows.Forms.PaintEventHandler(this.panelTop_Paint);
            // 
            // txtURL
            // 
            this.txtURL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtURL.LeftImage = global::Gmei.MdiDemo.Properties.Resources.blank;
            this.txtURL.Location = new System.Drawing.Point(133, 11);
            this.txtURL.Name = "txtURL";
            this.txtURL.RightImage = global::Gmei.MdiDemo.Properties.Resources.start;
            this.txtURL.Size = new System.Drawing.Size(234, 26);
            this.txtURL.TabIndex = 6;
            this.txtURL.LeftImageClick += new System.EventHandler(this.txtURL_LeftImageClick);
            this.txtURL.RightImageClick += new System.EventHandler(this.txtURL_RightImageClick);
            this.txtURL.TextKeyEnter += new System.EventHandler(this.txtURL_TextKeyEnter);
            // 
            // btnMenu
            // 
            this.btnMenu.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnMenu.BackgroundImage = global::Gmei.MdiDemo.Properties.Resources.menu;
            this.btnMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnMenu.FlatAppearance.BorderSize = 0;
            this.btnMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu.Location = new System.Drawing.Point(371, 10);
            this.btnMenu.Name = "btnMenu";
            this.btnMenu.Size = new System.Drawing.Size(27, 27);
            this.btnMenu.TabIndex = 5;
            this.btnMenu.UseVisualStyleBackColor = true;
            this.btnMenu.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // btnHome
            // 
            this.btnHome.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnHome.BackgroundImage = global::Gmei.MdiDemo.Properties.Resources.home;
            this.btnHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnHome.FlatAppearance.BorderSize = 0;
            this.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHome.Location = new System.Drawing.Point(99, 10);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(27, 27);
            this.btnHome.TabIndex = 4;
            this.btnHome.UseVisualStyleBackColor = true;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // btnStopOrReflesh
            // 
            this.btnStopOrReflesh.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnStopOrReflesh.BackgroundImage = global::Gmei.MdiDemo.Properties.Resources.reflesh;
            this.btnStopOrReflesh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnStopOrReflesh.FlatAppearance.BorderSize = 0;
            this.btnStopOrReflesh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStopOrReflesh.Location = new System.Drawing.Point(68, 10);
            this.btnStopOrReflesh.Name = "btnStopOrReflesh";
            this.btnStopOrReflesh.Size = new System.Drawing.Size(27, 27);
            this.btnStopOrReflesh.TabIndex = 3;
            this.btnStopOrReflesh.UseVisualStyleBackColor = true;
            this.btnStopOrReflesh.Click += new System.EventHandler(this.btnStopOrReflesh_Click);
            // 
            // btnForward
            // 
            this.btnForward.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnForward.BackgroundImage = global::Gmei.MdiDemo.Properties.Resources.forward;
            this.btnForward.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnForward.FlatAppearance.BorderSize = 0;
            this.btnForward.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnForward.Location = new System.Drawing.Point(38, 10);
            this.btnForward.Name = "btnForward";
            this.btnForward.Size = new System.Drawing.Size(27, 27);
            this.btnForward.TabIndex = 2;
            this.btnForward.UseVisualStyleBackColor = true;
            this.btnForward.Click += new System.EventHandler(this.btnForward_Click);
            // 
            // btnBack
            // 
            this.btnBack.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnBack.BackgroundImage = global::Gmei.MdiDemo.Properties.Resources.back;
            this.btnBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnBack.FlatAppearance.BorderSize = 0;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBack.Location = new System.Drawing.Point(8, 10);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(27, 27);
            this.btnBack.TabIndex = 1;
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // menuMain
            // 
            this.menuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnItemNewTab,
            this.mnItemGoHome,
            this.toolStripSeparator1,
            this.mnItemRecentlyView,
            this.toolStripSeparator2,
            this.mnItemAbout});
            this.menuMain.Name = "menuMain";
            this.menuMain.Size = new System.Drawing.Size(137, 104);
            // 
            // mnItemNewTab
            // 
            this.mnItemNewTab.Name = "mnItemNewTab";
            this.mnItemNewTab.Size = new System.Drawing.Size(136, 22);
            this.mnItemNewTab.Text = "新建空白页";
            this.mnItemNewTab.Click += new System.EventHandler(this.mnItemNewTab_Click);
            // 
            // mnItemGoHome
            // 
            this.mnItemGoHome.Name = "mnItemGoHome";
            this.mnItemGoHome.Size = new System.Drawing.Size(136, 22);
            this.mnItemGoHome.Text = "回到主页";
            this.mnItemGoHome.Click += new System.EventHandler(this.mnItemGoHome_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(133, 6);
            // 
            // mnItemRecentlyView
            // 
            this.mnItemRecentlyView.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnItemClearView,
            this.mnStoneHistory});
            this.mnItemRecentlyView.Name = "mnItemRecentlyView";
            this.mnItemRecentlyView.Size = new System.Drawing.Size(136, 22);
            this.mnItemRecentlyView.Text = "最近浏览";
            // 
            // mnItemClearView
            // 
            this.mnItemClearView.Name = "mnItemClearView";
            this.mnItemClearView.Size = new System.Drawing.Size(124, 22);
            this.mnItemClearView.Text = "清除记录";
            this.mnItemClearView.Click += new System.EventHandler(this.mnItemClearView_Click);
            // 
            // mnStoneHistory
            // 
            this.mnStoneHistory.Name = "mnStoneHistory";
            this.mnStoneHistory.Size = new System.Drawing.Size(121, 6);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(133, 6);
            // 
            // mnItemAbout
            // 
            this.mnItemAbout.Name = "mnItemAbout";
            this.mnItemAbout.Size = new System.Drawing.Size(136, 22);
            this.mnItemAbout.Text = "关于...";
            this.mnItemAbout.Click += new System.EventHandler(this.mnItemAbout_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(514, 228);
            this.Controls.Add(this.panelTop);
            this.IsMdiContainer = true;
            this.Name = "frmMain";
            this.Text = "Google Chrome Browser Demo";
            this.MdiBarCreated += new System.EventHandler(this.frmMain_MdiBarCreated);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.MdiChildActivate += new System.EventHandler(this.frmMain_MdiChildActivate);
            this.Resize += new System.EventHandler(this.frmMain_Resize);
            this.panelTop.ResumeLayout(false);
            this.menuMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.Button btnStopOrReflesh;
        private System.Windows.Forms.Button btnForward;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnMenu;
        private GMText txtURL;
        private System.Windows.Forms.ContextMenuStrip menuMain;
        private System.Windows.Forms.ToolStripMenuItem mnItemNewTab;
        private System.Windows.Forms.ToolStripMenuItem mnItemGoHome;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem mnItemRecentlyView;
        private System.Windows.Forms.ToolStripMenuItem mnItemClearView;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem mnItemAbout;
        private System.Windows.Forms.ToolStripSeparator mnStoneHistory;
    }
}
