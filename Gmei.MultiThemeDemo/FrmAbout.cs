﻿using System;
using System.Windows.Forms;

namespace Gmei.MultiThemeDemo
{
    public partial class FrmAbout : Gdu.WinFormUI.GMForm
    {
        public FrmAbout()
        {
            InitializeComponent();

            MaximizeBox = false;
            MinimizeBox = false;
            ShowInTaskbar = false;
            Resizable = false;
        }

        protected override void OnKeyUp(KeyEventArgs e)
        {
            base.OnKeyUp(e);
            if (e.KeyData == Keys.Escape)
                base.Close();
        }
    }
}
