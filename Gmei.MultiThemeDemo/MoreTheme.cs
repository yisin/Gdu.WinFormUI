﻿using System;
using System.Drawing;

using Gdu.WinFormUI.MyGraphics;
using Gdu.WinFormUI;

namespace Gmei.MultiThemeDemo
{
    public class ThemeMac : ThemeFormVS2013
    {
        public ThemeMac()
            : base()
        {
            ThemeName = "A mac look theme by user";
            IconSize = new Size(16, 16);
            FormBorderOutterColor = Color.FromArgb(101, 101, 101);
            FormBorderInnerColor = Color.FromArgb(120, Color.White);
            BorderWidth = 2;
            CaptionHeight = 28;
            CaptionBackColorTop = Color.FromArgb(230, 230, 230);
            CaptionBackColorBottom = Color.FromArgb(205, 205, 205);
            FormBackColor = Color.FromArgb(240, 240, 240);
            CaptionTextCenter = true;
            RoundedStyle = RoundStyle.Top;
            Radius = 8;

            CloseBoxSize = MaxBoxSize = MinBoxSize = new Size(16, 16);
            ControlBoxOffset = new Point(12, 8);
            ControlBoxSpace = 6;

            CloseBoxBackImageNormal = Properties.Resources.CloseNormal;
            CloseBoxBackImageHover = Properties.Resources.CloseHover;
            CloseBoxBackImagePressed = Properties.Resources.CloseDown;

            MaxBoxBackImageNormal = Properties.Resources.MaxNormal;
            MaxBoxBackImageHover = Properties.Resources.MaxHover;
            MaxBoxBackImagePressed = Properties.Resources.MaxDown;

            ResBoxBackImageNormal = Properties.Resources.ResNormal;
            ResBoxBackImageHover = Properties.Resources.ResHover;
            ResBoxBackImagePressed = Properties.Resources.ResDown;

            MinBoxBackImageNormal = Properties.Resources.MinNormal;
            MinBoxBackImageHover = Properties.Resources.MinHover;
            MinBoxBackImagePressed = Properties.Resources.MinDown;

        }
    }

    public class ThemeShadow : ThemeFormVS2013
    {
        public ThemeShadow()
            : base()
        {
            ThemeName = "Shadow form demo";
            IconSize = new Size(24, 24);
            IconLeftMargin = 8;
            BorderWidth = 0;
            CaptionHeight = 42;
            CaptionBackColorTop = Color.FromArgb(123,79,202);
            CaptionBackColorBottom = Color.FromArgb(123, 79, 202);
            FormBackColor = Color.FromArgb(242,242,242);
            CaptionTextColor = Color.White;

            // shadow
            SideResizeWidth = 0;
            ShowShadow = true;
            ShadowWidth = 6;
            ShadowColor = Color.Black;
            ShadowAValueDark = 50;
            ShadowAValueLight = 0;
            UseShadowToResize = true;

            CloseBoxSize = MaxBoxSize = MinBoxSize = new Size(30, 23);
            ControlBoxOffset = new Point(0, 0);
            ControlBoxSpace = 0;

            ButtonColorTable closeTable = new ButtonColorTable();
            closeTable.ForeColorNormal = closeTable.ForeColorHover
                = closeTable.ForeColorPressed = Color.FromArgb(249, 240, 223);
            closeTable.BackColorHover = Color.FromArgb(217, 71, 71);
            closeTable.BackColorPressed = Color.FromArgb(188, 52, 52);
            CloseBoxColor = closeTable;

            ButtonColorTable minTable = new ButtonColorTable();
            minTable.ForeColorNormal = minTable.ForeColorHover
                = minTable.ForeColorPressed = Color.FromArgb(249, 240, 223);
            minTable.BackColorHover = Color.FromArgb(67, 139, 221);
            minTable.BackColorPressed = Color.FromArgb(50, 125, 210);
            MinBoxColor = MaxBoxColor = minTable;
        }
    }
}
