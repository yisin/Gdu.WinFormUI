﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

using Gdu.WinFormUI;

namespace Gmei.MultiThemeDemo
{
    public partial class frmMain : Gdu.WinFormUI.GMForm
    {
        string oldText;

        public frmMain()
        {
            InitializeComponent();
            oldText = base.Text;
            base.XTheme = new ThemeMac();
        }

        private void ChangeTheme(ThemeFormBase newTheme)
        {
            bool needToGoBack = false;
            if (WindowState == FormWindowState.Maximized)
            {
                WindowState = FormWindowState.Normal;
                needToGoBack = true;
            }

            // 注意更换主题需要在normal状态下进行，在最大化状态下进行
            // 会使窗体边框的隐藏不正确
            base.XTheme = newTheme;
            base.Text = oldText + " [" + newTheme.ThemeName + "]";
            if (needToGoBack)
            {
                Application.DoEvents();
                System.Threading.Thread.Sleep(400);
                WindowState = FormWindowState.Maximized;
            }
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            cmbTheme.Items.Add("Default theme");
            cmbTheme.Items.Add("A new theme");
            cmbTheme.Items.Add("Dev Express");
            cmbTheme.Items.Add("VS2013");
            cmbTheme.Items.Add("Mac theme");
            cmbTheme.Items.Add("Use Shadow");
            cmbTheme.SelectedIndex = 4;

            panelMain.Dock = DockStyle.Fill;
            panelMain.BackColor = Color.Transparent;

            //base.TopMost = true;
        }

        private void cmbTheme_SelectedIndexChanged(object sender, EventArgs e)
        {
            ThemeFormBase theme = null;
            switch (cmbTheme.SelectedIndex)
            {
                case 0:
                    theme = new ThemeFormBase();
                    break;
                case 1:
                    theme = new ThemeFormNew();
                    break;
                case 2:
                    theme = new ThemeFormDevExpress();
                    break;
                case 3:
                    theme = new ThemeFormVS2013();
                    break;
                case 4:
                    theme = new ThemeMac();
                    break;
                case 5:
                    theme = new ThemeShadow();
                    break;
                default:
                    theme = new ThemeFormBase();
                    break;
            }
            ChangeTheme(theme);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FrmAbout about = new FrmAbout();
            
            about.XTheme = base.XTheme;
            about.Size = new Size(340, 190);
            if (base.XTheme.ShowShadow)
                about.Show(this);
            else
                about.ShowDialog();
        }
    }
}
